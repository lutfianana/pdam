package com.dinustek.pdamsemarang.model;

/**
 * Created by ASA on 6/20/2017.
 */

public class item_berita {
    public String NoID;
    public String Judul;
    public String Waktu;
    public String Konten;

    public String getKonten() {
        return Konten;
    }

    public String getWaktu() {
        return Waktu;
    }

    public void setWaktu(String waktu) {
        Waktu = waktu;
    }

    public String getJudul() {

        return Judul;
    }

    public void setKonten(String konten) {
        Konten = konten;
    }

    public void setJudul(String judul) {
        Judul = judul;
    }

    public String getNoID() {

        return NoID;
    }

    public void setNoID(String noID) {
        NoID = noID;
    }
}
