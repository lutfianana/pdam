package com.dinustek.pdamsemarang.model;

/**
 * Created by ASA on 7/7/2017.
 */

public class item_gangguan_air {
    public String Judul;
    public String Konten;
    public String Id;
    public String Lokasi;
    public String Tanggal;

    public void setLokasi(String lokasi) {
        Lokasi = lokasi;
    }

    public void setTanggal(String tanggal) {
        Tanggal = tanggal;
    }

    public void setJudul(String judul) {
        Judul = judul;
    }

    public void setKonten(String konten) {
        Konten = konten;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getJudul() {
        return Judul;
    }

    public String getKonten() {
        return Konten;
    }

    public String getId() {
        return Id;
    }

    public String getLokasi() {
        return Lokasi;
    }

    public String getTanggal() {
        return Tanggal;
    }
}
