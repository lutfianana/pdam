package com.dinustek.pdamsemarang.model;

/**
 * Created by ASA on 10/12/2017.
 */

public class item_tagihan_belum {
    public String ID;
    public String Bulan;
    public String Harga;
    public String Periode;

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setBulan(String bulan) {
        Bulan = bulan;
    }

    public void setHarga(String harga) {
        Harga = harga;
    }

    public void setPeriode(String periode) {
        Periode = periode;
    }

    public String getID() {
        return ID;
    }

    public String getBulan() {
        return Bulan;
    }

    public String getHarga() {
        return Harga;
    }

    public String getPeriode() {
        return Periode;
    }
}
