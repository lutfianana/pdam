package com.dinustek.pdamsemarang.model;

/**
 * Created by ASA on 10/14/2017.
 */

public class item_info_pelanggan {
    public String Nama;
    public String URL;

    public void setNama(String nama) {
        Nama = nama;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getNama() {
        return Nama;
    }

    public String getURL() {
        return URL;
    }
}
