package com.dinustek.pdamsemarang.model;

/**
 * Created by ASA on 10/20/2017.
 */

public class item_aduanku {
    public String Nama;
    public String Status;
    public String Laporan;
    public String Foto;
    public String Tanggal;
    public String Kategori;

    public void setNama(String nama) {
        Nama = nama;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public void setLaporan(String laporan) {
        Laporan = laporan;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public void setTanggal(String tanggal) {
        Tanggal = tanggal;
    }

    public void setKategori(String kategori) {
        Kategori = kategori;
    }

    public String getNama() {
        return Nama;
    }

    public String getStatus() {
        return Status;
    }

    public String getLaporan() {
        return Laporan;
    }

    public String getFoto() {
        return Foto;
    }

    public String getTanggal() {
        return Tanggal;
    }

    public String getKategori() {
        return Kategori;
    }
}
