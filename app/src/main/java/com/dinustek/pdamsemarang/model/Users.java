package com.dinustek.pdamsemarang.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ASA on 10/10/2017.
 */

public class Users extends RealmObject {

    private String Fcm_Token;
    private String Login_Token;
    private String No_Pelanggan;
    private String Nama;
    private String Alamat;
    private String No_Meter;

    public void setFcm_Token(String fcm_Token) {
        Fcm_Token = fcm_Token;
    }

    public void setLogin_Token(String login_Token) {
        Login_Token = login_Token;
    }

    public void setNo_Pelanggan(String no_Pelanggan) {
        No_Pelanggan = no_Pelanggan;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public void setAlamat(String alamat) {
        Alamat = alamat;
    }

    public void setNo_Meter(String no_Meter) {
        No_Meter = no_Meter;
    }

    public String getFcm_Token() {
        return Fcm_Token;
    }

    public String getLogin_Token() {
        return Login_Token;
    }

    public String getNo_Pelanggan() {
        return No_Pelanggan;
    }

    public String getNama() {
        return Nama;
    }

    public String getAlamat() {
        return Alamat;
    }

    public String getNo_Meter() {
        return No_Meter;
    }
}
