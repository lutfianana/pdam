package com.dinustek.pdamsemarang.model;

/**
 * Created by ASA on 6/1/2017.
 */

public class item_info {
    public String Name;
    public String NoId;

    public String getName() {
        return Name;
    }

    public String getNoId() {
        return NoId;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setNoId(String noId) {
        NoId = noId;
    }
}
