package com.dinustek.pdamsemarang.fragmentsupport;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.activity.LoginActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class NeedLoginFragment extends Fragment {
    View rootView;
    Button btn_Login;

    public NeedLoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_need_login, container, false);
        btn_Login = (Button)rootView.findViewById(R.id.NL_Login);
        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), LoginActivity.class);
                getActivity().startActivity(i);
                getActivity().finish();
            }
        });

        return rootView;
    }

}
