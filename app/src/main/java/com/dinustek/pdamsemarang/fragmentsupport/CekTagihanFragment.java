package com.dinustek.pdamsemarang.fragmentsupport;


import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.activity.DetailTagihanActivity;
import com.dinustek.pdamsemarang.activity.LoginActivity;
import com.dinustek.pdamsemarang.activity.TampungActivity;
import com.dinustek.pdamsemarang.helper.SessionManager;
import com.dinustek.pdamsemarang.helper.UserHelper;
import com.dinustek.pdamsemarang.helper.helper;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class CekTagihanFragment extends Fragment {
    View rootView;
    RelativeLayout CTP_RL_Bulan, CTP_RL_Tahun;
    private static String[] CHOOSE_FILE = {"January", "February", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
    private static String[] KODE_MONTH = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
    private static String[] TAHUN = new String[2];
    String NoPelanggan = "", Bulan = "", Tahun = "";
    TextView CTP_tvBulan, CTP_tvTahun;
    EditText CTP_edNoPelanggan;
    Button btnProsesCekTagihan;
    int YEAR, MONTH;
    CardView CT_btn_Gangguan, CT_btn_Berita, CT_btn_Kontak, CT_btn_Login, CT_btn_Pengaduan;
    LinearLayout LL_Putih;
    SessionManager session;

    public CekTagihanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new SessionManager(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_cek_tagihan, container, false);

        InitViews();
        getTanggal();

        if (session.isLoggedIn()){
            LL_Putih.setVisibility(View.GONE);
            CTP_edNoPelanggan.setText(UserHelper.NoPelanggan);
        }
        return rootView;
    }
    private void getTanggal(){
        Calendar calendar = Calendar.getInstance();
        YEAR = calendar.get(Calendar.YEAR);
        MONTH = calendar.get(Calendar.MONTH);

        CTP_tvBulan.setText(helper.getNamaBulan(MONTH));
        CTP_tvTahun.setText(String.valueOf(YEAR));

        TAHUN[0] = ""+(YEAR-1);
        TAHUN[1] = ""+(YEAR);

        if (MONTH<10){
            Bulan = "0"+MONTH;
        }else {
            Bulan = ""+MONTH;
        }

        Tahun = ""+YEAR;

        //Toast.makeText(getActivity(), Bulan + " - " +Tahun, Toast.LENGTH_SHORT).show();
    }
    private void InitViews(){
        btnProsesCekTagihan = (Button)rootView.findViewById(R.id.btnProsesCekTagihan);
        btnProsesCekTagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getNoPelanggan();
                if (DataValid()){
                    gotoTagihan();
                }
            }
        });
        CTP_tvTahun = (TextView)rootView.findViewById(R.id.CTP_tvTahun);
        CTP_tvBulan = (TextView)rootView.findViewById(R.id.CTP_tvBulan);
        CTP_edNoPelanggan = (EditText)rootView.findViewById(R.id.CTP_edNoPelanggan);
        CTP_edNoPelanggan.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.form_garis), PorterDuff.Mode.SRC_IN);
        CTP_edNoPelanggan.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                // If the event is a key-down event on the "enter" button
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) &&
                        (i == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    getNoPelanggan();
                    if (DataValid()){
                        gotoTagihan();
                    }else {
                        Toast.makeText(getActivity(), "Lengkapi Data", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });
        CTP_RL_Bulan = (RelativeLayout)rootView.findViewById(R.id.CTP_RL_Bulan);
        CTP_RL_Bulan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBulan();
            }
        });

        CTP_RL_Tahun = (RelativeLayout)rootView.findViewById(R.id.CTP_RL_Tahun);
        CTP_RL_Tahun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTahun();
            }
        });

        CT_btn_Pengaduan = (CardView)rootView.findViewById(R.id.CT_btn_Pengaduan);
        CT_btn_Pengaduan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BukaHalaman(7);
            }
        });
        CT_btn_Gangguan = (CardView)rootView.findViewById(R.id.CT_btn_Gangguan);
        CT_btn_Gangguan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BukaHalaman(3);
            }
        });
        CT_btn_Berita = (CardView)rootView.findViewById(R.id.CT_btn_Berita);
        CT_btn_Berita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BukaHalaman(5);
            }
        });
        CT_btn_Kontak = (CardView)rootView.findViewById(R.id.CT_btn_Kontak);
        CT_btn_Kontak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BukaHalaman(6);
            }
        });
        CT_btn_Login = (CardView)rootView.findViewById(R.id.CT_btn_Login);
        CT_btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), LoginActivity.class);
                getActivity().startActivity(i);
            }
        });

        LL_Putih = (LinearLayout)rootView.findViewById(R.id.LL_Putih);

    }
    private boolean DataValid(){
        if (Bulan.equals("")){
            return false;
        }else if(NoPelanggan.equals("")){
            return false;
        }else {
            return true;
        }
    }
    private void BukaHalaman(int page){
        Intent i = new Intent(getActivity().getApplicationContext(), TampungActivity.class);
        i.putExtra("page", page);
        startActivity(i);
    }
    private void getNoPelanggan(){
        NoPelanggan = CTP_edNoPelanggan.getText().toString();
    }
    private void dialogBulan(){
        new MaterialDialog.Builder(getActivity())
                .title("Pilih Bulan")
                .items(CHOOSE_FILE)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        //Toast.makeText(getActivity(), ""+text, Toast.LENGTH_SHORT).show();
                        CTP_tvBulan.setText(text);
                        Bulan = KODE_MONTH[position];
                    }
                })
                .show();
    }
    private void dialogTahun(){
        new MaterialDialog.Builder(getActivity())
                .title("Pilih Bulan")
                .items(TAHUN)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        //Toast.makeText(getActivity(), ""+text, Toast.LENGTH_SHORT).show();
                        CTP_tvTahun.setText(text);
                        Tahun = TAHUN[position];
                    }
                })
                .show();
    }
    private void gotoTagihan(){
        Intent i = new Intent(getActivity(), DetailTagihanActivity.class);
        i.putExtra("bulan", Bulan);
        i.putExtra("tahun", Tahun);
        i.putExtra("nopel", NoPelanggan);
        getActivity().startActivity(i);

    }
}
