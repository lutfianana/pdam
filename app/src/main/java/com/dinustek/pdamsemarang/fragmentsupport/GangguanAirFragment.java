package com.dinustek.pdamsemarang.fragmentsupport;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.adapter.adapter_berita;
import com.dinustek.pdamsemarang.adapter.adapter_gangguan_air;
import com.dinustek.pdamsemarang.controller.AppConfig;
import com.dinustek.pdamsemarang.controller.AppController;
import com.dinustek.pdamsemarang.helper.UserHelper;
import com.dinustek.pdamsemarang.model.item_berita;
import com.dinustek.pdamsemarang.model.item_gangguan_air;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class GangguanAirFragment extends Fragment {
    View rootView;
    ProgressDialog pDialog;

    //init recycler
    protected GangguanAirFragment.LayoutManagerType mCurrentLayoutManagerType;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }
    private static final int SPAN_COUNT = 2;
    protected RecyclerView mRecyclerView;
    protected adapter_gangguan_air mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    private List<item_gangguan_air> Item_Gangguan = new ArrayList<item_gangguan_air>();
    private static final String TAG = "H";
    Bundle savedInstance;

    public GangguanAirFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialog = new ProgressDialog(getActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        savedInstance = savedInstanceState;

        rootView = inflater.inflate(R.layout.fragment_gangguan_air, container, false);
        InitRecycler();

        Req_GangguanAir();
        return rootView;
    }
    private void InitRecycler(){
        //rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_GangguanAir);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mLayoutManager = new LinearLayoutManager(getActivity());

        mCurrentLayoutManagerType = GangguanAirFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstance != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (GangguanAirFragment.LayoutManagerType) savedInstance
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);

        mAdapter = new adapter_gangguan_air(getActivity(), Item_Gangguan);
        // Set CustomAdapter as the adapter for RecyclerView.
        mRecyclerView.setAdapter(mAdapter);

//        SampleData(20);
    }
    private void SampleData(int length){
        for (int i=0; i<length; i++){
            item_gangguan_air item = new item_gangguan_air();

            item.setId(""+i);
            item.setJudul("Judul"+i);
            item.setKonten("Konten "+i);

            Item_Gangguan.add(item);
        }
        mAdapter.notifyDataSetChanged();
    }
    private void PasangData(JSONArray data){
        try {
            for (int i=0; i<data.length(); i++){
                item_gangguan_air item = new item_gangguan_air();

                item.setId(data.getJSONObject(i).getString("id"));
                item.setJudul(data.getJSONObject(i).getString("judul"));
                item.setKonten(data.getJSONObject(i).getString("konten"));
                item.setLokasi(data.getJSONObject(i).getString("lokasi"));
                item.setTanggal(data.getJSONObject(i).getString("tanggal"));

                Item_Gangguan.add(item);
            }
            mAdapter.notifyDataSetChanged();
        }catch (JSONException e){
            Toast.makeText(getActivity(), ""+e, Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void Req_GangguanAir(){
        showPDialog("Loading ...");
        // Creating volley request obj
        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_GangguanAir, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                // Parsing json
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    if(!error){
                        PasangData(jObj.getJSONArray("data"));
                    }else{
                        Toast.makeText(getActivity(), jObj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
                Toast.makeText(getActivity().getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                finish();

            }
        });

        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 10, 1.0f));
        AppController.getInstance().addToRequestQueue(strReq);
    }

    public void setRecyclerViewLayoutManager(GangguanAirFragment.LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mCurrentLayoutManagerType = GangguanAirFragment.LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = GangguanAirFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            case STRAGGEREDGRID_LAYOUT_MANAGER:
                mLayoutManager = new StaggeredGridLayoutManager(SPAN_COUNT, StaggeredGridLayoutManager.VERTICAL);
                mCurrentLayoutManagerType = GangguanAirFragment.LayoutManagerType.STRAGGEREDGRID_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = GangguanAirFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }
}
