package com.dinustek.pdamsemarang.controller;


public class AppConfig {

    //public static String HOST = "http://66.96.244.85/pdam-ws/";
    public static String HOST = "http://app.pdamkotasmg.co.id/";
    //public static String HOST = "http://devel.dinustech.com/pdam-ws/";
    public static String URL_Login = HOST+"pdam-ws/api/login";
    public static String URL_Profil = HOST+"pdam-ws/api/profil";
    public static String URL_Berita = HOST+"pdam-ws/api/berita";
    public static String URL_GangguanAir = HOST+"pdam-ws/api/gangguan";
    public static String URL_Pengaduan = HOST+"pdam-ws/api/pengaduan";//kategori, long, lat,token, laporan, kontak, foto
    public static String URL_ListPengaduan = HOST+"pdam-ws/api/listpengaduan";//token
    public static String URL_RangeTagihan = HOST+"pdam-ws/api/rangetagihan";//token, tahun1, bulan1, tahun2, bulan2
    public static String URL_TagihanBulan = HOST+"pdam-ws/api/tagihan";
    public static String URL_TagihanPublik = HOST+"pdam-ws/api/tagihanpublic";
    public static String URL_MeterBulanan = HOST+"pdam-ws/api/meterbulanan";
    public static String URL_GetKategori = HOST+"pdam-ws/api/kategori";
    public static String URL_Registrasi = HOST+"humas/api/register";
    public static String URL_SendKode = HOST+"humas/api/confirm";
    public static String URL_TotalTagihan = HOST+"api/totaltagihan";//token
    public static String URL_Slider = "https://www.pdamkotasmg.co.id/api/pdam";
    public static String Apikey = "PDAW6444Semarang";

}
