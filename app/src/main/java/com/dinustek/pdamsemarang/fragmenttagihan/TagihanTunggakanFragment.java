package com.dinustek.pdamsemarang.fragmenttagihan;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.activity.DetailTagihanActivity;
import com.dinustek.pdamsemarang.adapter.adapter_tagihan_belum;
import com.dinustek.pdamsemarang.controller.AppConfig;
import com.dinustek.pdamsemarang.controller.AppController;
import com.dinustek.pdamsemarang.helper.UserHelper;
import com.dinustek.pdamsemarang.helper.helper;
import com.dinustek.pdamsemarang.model.item_tagihan_belum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class TagihanTunggakanFragment extends Fragment {
    View rootView;
    RelativeLayout TagApril;
    ProgressDialog pDialog;
    String TAG = "BelumTerbayar";
    TextView tvBulan, tvHarga;
    LinearLayout LL_No_Tagihan, LL_Parent;
    String strtahun;
    SimpleDateFormat dtahun;
    Date today;

    //init recycler
    protected TagihanTunggakanFragment.LayoutManagerType mCurrentLayoutManagerType;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }
    private static final int SPAN_COUNT = 2;
    protected RecyclerView mRecyclerView;
    protected adapter_tagihan_belum mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    private List<item_tagihan_belum> Item_Data = new ArrayList<item_tagihan_belum>();
    Bundle savedInstance;

    int JumlahTotalBayar = 0;
    TextView tvJumlahTotal;


    public TagihanTunggakanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialog = new ProgressDialog(getActivity());
        /*
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/LobsterTwo_BoldItalic.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        */
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_tagihan_tunggakan, container, false);

        today = Calendar.getInstance().getTime();
        dtahun = new SimpleDateFormat("yyyy");
        strtahun = dtahun.format(today);

        InitViews();
        InitRecycler();
        Req_RangeTagihan();
        return rootView;
    }
    private void InitViews(){
        LL_No_Tagihan = (LinearLayout)rootView.findViewById(R.id.LL_Tagihan_Belum_Kosong);
        LL_Parent = (LinearLayout)rootView.findViewById(R.id.LL_Tagihan_Belum_Parent);
        tvJumlahTotal = (TextView)rootView.findViewById(R.id.tvJumlahTotal);
//        TagApril = (RelativeLayout)rootView.findViewById(R.id.);
//        TagApril.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(getActivity(), DetailTagihanActivity.class);
//                i.putExtra("tahun", "2017");
//                i.putExtra("bulan", "04");
//                i.putExtra("bayar", false);
//                startActivity(i);
//            }
//        });
    }
    private void InitRecycler(){
        //rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_belum_terbayar);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mLayoutManager = new LinearLayoutManager(getActivity());

        mCurrentLayoutManagerType = TagihanTunggakanFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstance != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (TagihanTunggakanFragment.LayoutManagerType) savedInstance
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);

        mAdapter = new adapter_tagihan_belum(getActivity(), Item_Data);
        // Set CustomAdapter as the adapter for RecyclerView.
        mRecyclerView.setAdapter(mAdapter);

    }
    private void PasangData(JSONArray item){
        try {
            Item_Data.clear();
            for (int i=0; i<item.length(); i++){
                JSONObject data = item.getJSONObject(i);

                item_tagihan_belum items = new item_tagihan_belum();

                if (data.getString("TGIBAYAR").equals("") || data.getString("TGIBAYAR").equals("null")){
                    items.setID("");
                    items.setBulan(helper.getNamaBulan(Integer.valueOf(data.getString("PERIODE").substring(4, 6))));
                    items.setHarga(helper.getRupiah(data.getInt("TOTALBYR")));
                    items.setPeriode(data.getString("PERIODE"));

                    JumlahTotalBayar = JumlahTotalBayar + data.getInt("TOTALBYR");

                    Item_Data.add(items);
                }
            }
            LL_Parent.setVisibility(View.VISIBLE);
            if (Item_Data.size()==0){
                LL_No_Tagihan.setVisibility(View.VISIBLE);
            }
            mAdapter.notifyDataSetChanged();
            tvJumlahTotal.setText(helper.getRupiah(JumlahTotalBayar));
        }catch (JSONException e){
            Toast.makeText(getActivity(), ""+e, Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void Req_RangeTagihan(){
        showPDialog("Loading ...");
        Log.d("TOKENN", UserHelper.UToken);
        // Creating volley request obj
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_RangeTagihan, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                // Parsing json
                try {
                    JSONObject jObj = new JSONObject(response);
                    Boolean error = jObj.getBoolean("error");

                    if(!error){
                        //JSONObject data = jObj.getJSONObject("data");
                        PasangData(jObj.getJSONArray("data"));

                        //DialogLoginPesan("Perhatian", "Login berhasil");
                    }else{
                        Toast.makeText(getActivity(), jObj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                finish();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("token", UserHelper.UToken);
                params.put("tahun1", "2019");
                params.put("bulan1", "01");
                params.put("tahun2", "2019");
                params.put("bulan2", "12");
                return params;
            }

        };

        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 10, 1.0f));
        AppController.getInstance().addToRequestQueue(strReq);
    }
    public void setRecyclerViewLayoutManager(TagihanTunggakanFragment.LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mCurrentLayoutManagerType = TagihanTunggakanFragment.LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = TagihanTunggakanFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            case STRAGGEREDGRID_LAYOUT_MANAGER:
                mLayoutManager = new StaggeredGridLayoutManager(SPAN_COUNT, StaggeredGridLayoutManager.VERTICAL);
                mCurrentLayoutManagerType = TagihanTunggakanFragment.LayoutManagerType.STRAGGEREDGRID_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = TagihanTunggakanFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }
}
