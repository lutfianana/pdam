package com.dinustek.pdamsemarang.helper;


import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.util.Log;

import com.dinustek.pdamsemarang.model.Users;

import io.realm.Realm;
import io.realm.RealmResults;


public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }


    /*Tambahanku*/
    // inc id Users
    public int getRealmUsersid(){
        // increment index
        Number currentIdNum = realm.where(Users.class).max("id");
        int nextId;
        if(currentIdNum == null) {
            nextId = 1;
        } else {
            nextId = currentIdNum.intValue() + 1;
        }
        return nextId;
    }
    //Insert Users
    public void AddUsersData(String fcm_token, String login_token, String no_pelanggan, String name, String alamat, String no_meter ){
        Users Users = new Users();
        Users.setFcm_Token(fcm_token);
        Users.setLogin_Token(login_token);
        Users.setNo_Pelanggan(no_pelanggan);
        Users.setNama(name);
        Users.setAlamat(alamat);
        Users.setNo_Meter(no_meter);

        realm.beginTransaction();
        realm.copyToRealm(Users);
        realm.commitTransaction();

        Log.d("Success Insert Realm", "data: "+realm.allObjects(Users.class).size());
    }
    //query a single item with the given id
    public Users getUsersById(int id) {

        return realm.where(Users.class).equalTo("id", id).findFirst();
    }
    public Users getFirstUser() {
        return realm.where(Users.class).findFirst();
    }
    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }

    //clear all objects from Users.class
    public void clearAllData() {

        realm.beginTransaction();
        realm.clear(Users.class);
        realm.commitTransaction();

        Log.d("Delete All Data", "Hapus Data Berhasil");

    }

    //find all objects in the Users.class
    public RealmResults<Users> getUsers() {

        return realm.where(Users.class).findAll();
    }


    //check if Users.class is empty
    public boolean isUserLogin() {
        Log.d("Total Data ", ""+realm.allObjects(Users.class).size());
        return !realm.allObjects(Users.class).isEmpty();
    }
}
