package com.dinustek.pdamsemarang.helper;

/**
 * Created by ASA on 1/23/2017.
 */

public interface Template {
    interface VolleyRetryPolicy{
        //Waktu upload hingga sebelum time out, jika koneksi lambat, perbesar nilai dibawah
        int SOCKET_TIMEOUT = 1000 * 100;
        //Berapa kali perulangan, setelah koneksi gagal
        int RETRIES = 0;
    }
    interface Params{
        String[] parameter = new String[]{"title", "msisdn","imei", "longitude", "latitude", "jnslapor", "nama", "token", "apikey"};
    }


    interface Query{
        //Penggunaan Key dan Value untuk Map<,> dan keperluan difile php
        String KEY_FILE = "file";
        String KEY_IMAGE = "image";
        String KEY_DIRECTORY = "directory";
        String VALUE_DIRECTORY = "Data";
        String KEY_CODE = "kode";
        String KEY_MESSAGE = "pesan";
        String VALUE_CODE_SUCCESS = "2";
        String VALUE_CODE_FAILED = "1";
        String VALUE_CODE_MISSING = "0";
    }

    interface Code{
        int CAMERA_IMAGE_CODE = 0;
        int CAMERA_VIDEO_CODE = 1;
        int FILE_MANAGER_CODE = 2;
        int ACCESS_FINE_LOCATION_INTENT_ID = 3;
        int GALLERY_KITKAT_INTENT_CALLED = 4;
        int AUDIO_CODE = 5;

    }
    interface JenisAkun{
        String AGEN = "3";
        String PELANGGAN = "4";
    }
    interface ParamExtra{
        String DETAILID = "detailid";
        String PAGE = "page";
        String JARAKTEMPUH = "jaraktempuh";
        String WAKTUTEMPUH = "waktutempuh";
        String USER_NAMA = "nama";
        String USER_NO_MEMBER = "nomember";
        String USER_NO_HP = "nohp";
        String USER_ALAMAT = "alamat";
    }
}
