package com.dinustek.pdamsemarang.helper;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by ASA on 10/10/2017.
 */

public class helper {
    public static String getRupiah(int number){
        Locale locale = new Locale("id","ID");
        NumberFormat formatKurensi = NumberFormat.getCurrencyInstance(locale);
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) formatKurensi).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");// menghilangkan  simbol kurensi
        ((DecimalFormat) formatKurensi).setDecimalFormatSymbols(decimalFormatSymbols);
        return "Rp. "+String.valueOf(formatKurensi.format(number))+",-";
    }
    public static String getCabang(int kode){
        String Cabang = "";
        if (kode == 1){
            Cabang = "Selatan";
        }else if (kode == 2){
            Cabang = "Barat";
        }else if (kode == 3){
            Cabang = "Timur";
        }else if (kode == 4){
            Cabang = "Utara";
        }else if (kode == 5){
            Cabang = "Tengah";
        }
        return Cabang;
    }
    public static String getBulanRekening(String s){
        String Tahun = s.substring(0, 4);
        String Bulan = s.substring(4, 6);
        return getNamaBulan(Integer.valueOf(Bulan)) +" "+Tahun;
    }
    public static String getNamaBulan(int bln){
        String BlnName = "";
        switch (bln){
            case 1: BlnName = "Januari"; break;
            case 2: BlnName = "Februari"; break;
            case 3: BlnName = "Maret"; break;
            case 4: BlnName = "April"; break;
            case 5: BlnName = "Mei"; break;
            case 6: BlnName = "Juni"; break;
            case 7: BlnName = "Juli"; break;
            case 8: BlnName = "Agustus"; break;
            case 9: BlnName = "September"; break;
            case 10: BlnName = "Oktober"; break;
            case 11: BlnName = "November"; break;
            case 12: BlnName = "Desember"; break;
        }
        return BlnName;
    }
    public static String getStatusBayar(String s){
        if (s.equals("") || s.equals("null")){
            return "Belum Terbayar";
        }else{
            return "Terbayar";
        }
    }
    public static String getStatusAduan(int s){
        if (s == 0){
            return "Belum Ditanggapi";
        }else{
            return "Telah Ditanggapi";
        }
    }
}
