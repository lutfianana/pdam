package com.dinustek.pdamsemarang.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.activity.MainTabActivity;
import com.dinustek.pdamsemarang.controller.AppConfig;
import com.dinustek.pdamsemarang.controller.AppController;
import com.dinustek.pdamsemarang.helper.UserHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class AkunFragment extends Fragment {
    View rootView;
    TextView tvNama, ttvNamaLang, tvNomer, tvNoMet, tvAlamat;
    ProgressDialog pDialog;
    String TAG = "AkunFragment";

    public AkunFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialog = new ProgressDialog(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_akun, container, false);
        InitViews();

        //Req_Profile();
        PasangData();
        return rootView;
    }
    private void InitViews(){
        tvNama = (TextView)rootView.findViewById(R.id.Akun_tvNama);
        ttvNamaLang = (TextView)rootView.findViewById(R.id.Akun_tvNamaLang);
        tvNoMet = (TextView)rootView.findViewById(R.id.Akun_tvNoMeter);
        tvNomer = (TextView)rootView.findViewById(R.id.Akun_tvNoLang);
        tvAlamat = (TextView)rootView.findViewById(R.id.Akun_tvAlamat);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void PasangData(){
        tvNama.setText(UserHelper.Nama);
        ttvNamaLang.setText(UserHelper.Nama);
        tvNomer.setText(UserHelper.NoPelanggan);
        tvNoMet.setText(UserHelper.NoMeter);
        tvAlamat.setText(UserHelper.Alamat);
    }
    private void Req_Profile(){
        showPDialog("Loading ...");
        // Creating volley request obj
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_Profil, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                // Parsing json
                try {
                    JSONObject jObj = new JSONObject(response);
                    Boolean error = jObj.getBoolean("error");
                    //boolean status = jObj.getBoolean("status");

                    if(!error){
                        JSONObject data = jObj.getJSONObject("data");

                        tvNama.setText(data.getString("NAMA"));
                        ttvNamaLang.setText(data.getString("NAMA"));
                        tvNomer.setText(data.getString("NOLANGG"));
                        tvNoMet.setText(data.getString("NOMORMETER"));
                        tvAlamat.setText(data.getString("ALAMAT"));
                    }else{
                        Toast.makeText(getActivity(), jObj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                finish();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("token", UserHelper.UToken);

                return params;
            }

        };

        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 10, 1.0f));
        AppController.getInstance().addToRequestQueue(strReq);
    }
    private void DialogLoginPesan(String judul, String pesan){
        new MaterialDialog.Builder(getActivity())
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent i = new Intent(getActivity(), MainTabActivity.class);
                        startActivity(i);

                    }
                })
                .show();
    }
    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(getActivity())
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .show();
    }
}
