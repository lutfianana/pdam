package com.dinustek.pdamsemarang.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.adapter.adapter_info_pelanggan;
import com.dinustek.pdamsemarang.controller.AppConfig;
import com.dinustek.pdamsemarang.controller.AppController;
import com.dinustek.pdamsemarang.model.item_info_pelanggan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class InfoPelangganFragment extends Fragment {
    View rootView;
    ProgressDialog pDialog;
    int[] Info_String = {R.string.info_0, R.string.info_1, R.string.info_2, R.string.info_3, R.string.info_4, R.string.info_5, R.string.info_6, R.string.info_7, R.string.info_8};
    String[] URL_String = {
            "http://www.pdamkotasmg.co.id/info-pelanggan/sb.html",
            "http://www.pdamkotasmg.co.id/info-pelanggan/koreksi.html",
            "http://www.pdamkotasmg.co.id/info-pelanggan/penutupan.html",
            "http://www.pdamkotasmg.co.id/info-pelanggan/bk.html",
            "http://www.pdamkotasmg.co.id/info-pelanggan/pembelian-air-tangki.html",
            "http://www.pdamkotasmg.co.id/info-pelanggan/balik-nama-ganti-tarip.html",
            "http://www.pdamkotasmg.co.id/info-pelanggan/pengaduan.html",
            "http://www.pdamkotasmg.co.id/info-pelanggan/pindah-taping-pelanggan.html",
            "http://www.pdamkotasmg.co.id/info-pelanggan/kantor-pelayanan.html"
    };

    //init recycler
    protected InfoPelangganFragment.LayoutManagerType mCurrentLayoutManagerType;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }
    private static final int SPAN_COUNT = 2;
    protected RecyclerView mRecyclerView;
    protected adapter_info_pelanggan mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    private List<item_info_pelanggan> ITEM_DATA = new ArrayList<item_info_pelanggan>();
    private static final String TAG = "InfoPelanggan";
    Bundle savedInstance;

    public InfoPelangganFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialog = new ProgressDialog(getActivity());
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_info_pelanggan, container, false);

        InitRecycler();

        PasangData();

        return rootView;
    }
    private void InitRecycler(){
        //rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_Info_Pelanggan);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mLayoutManager = new LinearLayoutManager(getActivity());

        mCurrentLayoutManagerType = InfoPelangganFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstance != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (InfoPelangganFragment.LayoutManagerType) savedInstance
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);

        mAdapter = new adapter_info_pelanggan(getActivity(), ITEM_DATA);
        // Set CustomAdapter as the adapter for RecyclerView.
        mRecyclerView.setAdapter(mAdapter);

    }
    private void PasangData(){
        try {
            ITEM_DATA.clear();
            for (int i=0; i<Info_String.length; i++){
                item_info_pelanggan item = new item_info_pelanggan();

                item.setNama(getString(Info_String[i]));
                item.setURL(URL_String[i]);

                ITEM_DATA.add(item);
            }
            mAdapter.notifyDataSetChanged();
        }catch (Exception e){
            Toast.makeText(getActivity(), ""+e, Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void Req_Info(){
        //showPDialog("Loading ...");
        // Creating volley request obj
        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_Berita, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                // Parsing json
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    if(!error){
                        //PasangData();
                    }else{
                        Toast.makeText(getActivity(), jObj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                finish();

            }
        });

        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 10, 1.0f));
        AppController.getInstance().addToRequestQueue(strReq);
    }
    public void setRecyclerViewLayoutManager(InfoPelangganFragment.LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mCurrentLayoutManagerType = InfoPelangganFragment.LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = InfoPelangganFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            case STRAGGEREDGRID_LAYOUT_MANAGER:
                mLayoutManager = new StaggeredGridLayoutManager(SPAN_COUNT, StaggeredGridLayoutManager.VERTICAL);
                mCurrentLayoutManagerType = InfoPelangganFragment.LayoutManagerType.STRAGGEREDGRID_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = InfoPelangganFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }

}
