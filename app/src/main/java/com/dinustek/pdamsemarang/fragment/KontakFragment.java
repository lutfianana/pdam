package com.dinustek.pdamsemarang.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.dinustek.pdamsemarang.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * A simple {@link Fragment} subclass.
 */
public class KontakFragment extends Fragment implements OnMapReadyCallback {
    MapView mapView;
    GoogleMap gMap;
    View rootView;
    ImageView btnCallCenter ,btnMail, btnFB, btnIG, btnTwitter, btnWebsite;

    public KontakFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_kontak, container, false);
            mapView = (MapView) rootView.findViewById(R.id.maps);
            InitViews();
            mapView.onCreate(savedInstanceState);
            if (mapView != null) {
                mapView.getMapAsync(this);
            }

        }

        return rootView;
    }
    private void InitViews(){
        btnCallCenter = (ImageView)rootView.findViewById(R.id.fk_img_kn4);
        btnMail = (ImageView)rootView.findViewById(R.id.fk_img_kn5);
        btnFB = (ImageView)rootView.findViewById(R.id.fk_img_kn6);
        btnFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Goto("https://www.facebook.com/tirtamoedal.pdamkotasemarang/");
            }
        });
        btnIG = (ImageView)rootView.findViewById(R.id.fk_img_kn8);
        btnIG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Goto("https://www.instagram.com/pdam_kota_semarang/");
            }
        });
        btnTwitter = (ImageView)rootView.findViewById(R.id.fk_img_kn7);
        btnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Goto("https://twitter.com/pdamkotasmg/");
            }
        });
        btnWebsite = (ImageView)rootView.findViewById(R.id.fk_img_kn9);
        btnWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Goto("http://www.pdamkotasmg.co.id/");
            }
        });
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null) {
            mapView.onDestroy();
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;

        if(mapView!=null){
            drawMarker("-7.001391", "110.401830", "PDAM Kota Semarang");
        }

    }
    private void drawMarker(String lat, String longi, String Title){
        Double Lat = Double.valueOf(lat);
        Double Long = Double.valueOf(longi);

        LatLng marker = new LatLng(Lat, Long);
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker, 16));
        gMap.addMarker(new MarkerOptions().title(Title).position(marker));
    }
    private void Goto(String s){
        Intent i = new Intent(Intent.ACTION_VIEW,
                Uri.parse(s));
        startActivity(i);

    }
}
