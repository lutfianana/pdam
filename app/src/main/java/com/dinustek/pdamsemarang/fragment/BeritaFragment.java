package com.dinustek.pdamsemarang.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.adapter.adapter_berita;
import com.dinustek.pdamsemarang.controller.AppConfig;
import com.dinustek.pdamsemarang.controller.AppController;
import com.dinustek.pdamsemarang.helper.UserHelper;
import com.dinustek.pdamsemarang.model.item_berita;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeritaFragment extends Fragment {
    View rootView;
    ProgressDialog pDialog;

    //init recycler
    protected LayoutManagerType mCurrentLayoutManagerType;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }
    private static final int SPAN_COUNT = 2;
    protected RecyclerView mRecyclerView;
    protected adapter_berita mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    private List<item_berita> Item_Berita = new ArrayList<item_berita>();
    private static final String TAG = "BeritaFragment";
    Bundle savedInstance;

    public BeritaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialog = new ProgressDialog(getActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        
        savedInstance = savedInstanceState;
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_berita, container, false);
        
        InitRecycler();

        Req_Berita();
        return rootView;
    }

    private void InitRecycler(){
        //rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_Berita);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mLayoutManager = new LinearLayoutManager(getActivity());

        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstance != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstance
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);

        mAdapter = new adapter_berita(getActivity(), Item_Berita);
        // Set CustomAdapter as the adapter for RecyclerView.
        mRecyclerView.setAdapter(mAdapter);

    }
    private void PasangData(JSONArray data){
        try {
            Item_Berita.clear();
            for (int i=0; i<data.length(); i++){
                JSONObject dataArray = null;

                dataArray = data.getJSONObject(i);

                item_berita item = new item_berita();

                item.setNoID(dataArray.getString("id"));
                item.setJudul(dataArray.getString("title"));
                item.setKonten(dataArray.getString("content"));
                item.setWaktu(dataArray.getString("time"));

                Item_Berita.add(item);
            }
            mAdapter.notifyDataSetChanged();
        }catch (JSONException e){
            Toast.makeText(getActivity(), ""+e, Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
            pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void Req_Berita(){
        //showPDialog("Loading ...");
        // Creating volley request obj
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_Slider, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String KodeRespon = jObj.getString("response");
                    if (KodeRespon.equals("00")){
                        JSONArray data = jObj.getJSONArray("data");
                        PasangData(data);



                    }else {
                        Toast.makeText(getActivity(),"error" , Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();
                //checking error koneksi
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("api_key", AppConfig.Apikey);
                params.put("type","berita");

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);
    }
    public void setRecyclerViewLayoutManager(BeritaFragment.LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mCurrentLayoutManagerType = BeritaFragment.LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = BeritaFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            case STRAGGEREDGRID_LAYOUT_MANAGER:
                mLayoutManager = new StaggeredGridLayoutManager(SPAN_COUNT, StaggeredGridLayoutManager.VERTICAL);
                mCurrentLayoutManagerType = BeritaFragment.LayoutManagerType.STRAGGEREDGRID_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = BeritaFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }
}
