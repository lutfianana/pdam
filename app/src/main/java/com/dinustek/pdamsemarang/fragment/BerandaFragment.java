package com.dinustek.pdamsemarang.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.transition.TransitionManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.activity.TampungActivity;
import com.dinustek.pdamsemarang.adapter.adapter_info;
import com.dinustek.pdamsemarang.controller.AppConfig;
import com.dinustek.pdamsemarang.controller.AppController;
import com.dinustek.pdamsemarang.helper.RealmController;
import com.dinustek.pdamsemarang.helper.SessionManager;
import com.dinustek.pdamsemarang.helper.Template;
import com.dinustek.pdamsemarang.helper.TransformerAdapter;
import com.dinustek.pdamsemarang.model.item_banner;
import com.dinustek.pdamsemarang.model.item_info;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.events.OnBannerClickListener;
import ss.com.bannerslider.views.BannerSlider;

/**
 * A simple {@link Fragment} subclass.
 */
public class BerandaFragment extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{
    View rootView;
    //slider
   // SliderLayout sliderShow;

    //TextSliderView textSliderView;

    DefaultSliderView textSliderView;
    int DURATION_SLIDER = 8;
    ViewGroup transitionsContainer;
    ImageView RL_ic_dropdown;
    boolean RL_ic_isRotated = false;
    RelativeLayout RL_Info;
    LinearLayout LL_Info;
    BannerSlider bannerSlider;

    protected BerandaFragment.LayoutManagerType mCurrentLayoutManagerType;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }
    private static final int SPAN_COUNT = 2;
    protected RecyclerView mRecyclerView;
    protected adapter_info mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    private List<item_info> Item_Berita = new ArrayList<item_info>();
    private static final String TAG = "RecyclerViewFragment";
    private ProgressDialog pDialog;
    RealmController RC;
    SessionManager session;

    private CardView btnTagihan, btnPengaduan, btnGangguan;
    public static BerandaFragment berandaFragment;

    private List<item_banner> ItemBanner = new ArrayList<>();
    //private int layout;

    public BerandaFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialog = new ProgressDialog(getActivity());
        //layout = getArguments().getInt("layout");
        berandaFragment = this;
        //Logout();
    }
    public static BerandaFragment getInstance(){
        return berandaFragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_beranda, container, false);

        InitSlider();

        btnPengaduan = (CardView)rootView.findViewById(R.id.btn_Pengaduan);
        btnPengaduan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BukaHalaman(2);
            }
        });
        btnTagihan = (CardView)rootView.findViewById(R.id.btn_Tagihan);
        btnTagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BukaHalaman(1);
            }
        });
        btnGangguan = (CardView)rootView.findViewById(R.id.btn_Gangguan);
        btnGangguan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BukaHalaman(3);
            }
        });



        return rootView;
    }

    /*@Override
    public void onStop() {
        sliderShow.stopAutoCycle();
        super.onStop();
    }*/
    /*private void setSilder(){
        String Base_URL = "http://66.96.244.85/pdam-ws/public/images/banner/";

        //custom looping
//        HashMap<String,String> url_maps = new HashMap<String, String>();
//        url_maps.put("HUT PDAM ke 106", Base_URL+"banner_andro01.jpg");
//        url_maps.put("a", Base_URL+"banner_andro02.jpg");
//        url_maps.put("b", Base_URL+"banner_andro03.jpg");
//        url_maps.put("c", Base_URL+"banner_andro04.jpg");
//        url_maps.put("d", Base_URL+"banner_andro05.jpg");

        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("HUT PDAM ke 106",R.drawable.banner_andro01e1);
        file_maps.put("a",R.drawable.banner_andro02e1);
        file_maps.put("b",R.drawable.banner_andro03e1);
        file_maps.put("c", R.drawable.banner_andro04e1);
        file_maps.put("d", R.drawable.banner_andro05e1);

        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
//                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            sliderShow.addSlider(textSliderView);
        }
        sliderShow.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderShow.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderShow.setCustomAnimation(new DescriptionAnimation());
        sliderShow.setDuration(DURATION_SLIDER*1000);
        sliderShow.addOnPageChangeListener(this);

        //default image with text description
        /*
        textSliderView
                .description("Game of Thrones")
                .image("http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");

        sliderShow.addSlider(textSliderView);

    }*/
    private void InitSlider(){
        //Banner Slider
        bannerSlider =  rootView.findViewById(R.id.slider);

        textSliderView = new DefaultSliderView(getContext());
        transitionsContainer = (ViewGroup)rootView.findViewById(R.id.transitions_container);
        Reqbanner();
        //setSilder();

        /*ListView l = (ListView)rootView.findViewById(R.id.transformers);
        l.setAdapter(new TransformerAdapter(getActivity()));
        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sliderShow.setPresetTransformer(((TextView) view).getText().toString());
                Toast.makeText(getContext(), ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });*/


    }




    private void BukaHalaman(int page){
        Intent i = new Intent(getActivity().getApplicationContext(), TampungActivity.class);
        i.putExtra("page", page);
        startActivity(i);
    }
    private void SampleData(int length){
        for (int i=0; i<length; i++){
            item_info item = new item_info();

            item.setNoId(""+(i));
            item.setName("Coba "+(i+1));

            Item_Berita.add(item);
        }
        mAdapter.notifyDataSetChanged();
    }
    private void Reqbanner(){
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_Slider, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String KodeRespon = jObj.getString("response");
                    if (KodeRespon.equals("00")){
                        JSONArray data = jObj.getJSONArray("data");
                        pasangslider(data);



                    }else {
                        Toast.makeText(getActivity(),"error" , Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();
                //checking error koneksi
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("api_key", AppConfig.Apikey);
                params.put("type","slider");

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);
    }





    private void RotateDropDown(){
        TransitionManager.beginDelayedTransition(transitionsContainer);
        RL_ic_dropdown.setRotation(RL_ic_isRotated ? 0 : 180);
        if (RL_ic_isRotated){
            RL_ic_isRotated = false;
            LL_Info.setVisibility(View.GONE);
        }else {
            LL_Info.setVisibility(View.VISIBLE);
            RL_ic_isRotated = true;
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
        //RecyleSlider();
    }

    private void  showPDialog(){
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(getActivity(), slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void setRecyclerViewLayoutManager(BerandaFragment.LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mCurrentLayoutManagerType = BerandaFragment.LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = BerandaFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            case STRAGGEREDGRID_LAYOUT_MANAGER:
                mLayoutManager = new StaggeredGridLayoutManager(SPAN_COUNT, StaggeredGridLayoutManager.VERTICAL);
                mCurrentLayoutManagerType = BerandaFragment.LayoutManagerType.STRAGGEREDGRID_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = BerandaFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }

    private void pasangslider(JSONArray dataSlider){
        try {

            final List<Banner> banners=new ArrayList<>();
            //List<String> itemBanners = new ArrayList<>();
            for (int i=0; i<dataSlider.length(); i++){

                JSONObject data = null;
                data = dataSlider.getJSONObject(i);
                //add banner using image url
                banners.add(new RemoteBanner(data.getString("pict")).setScaleType(ImageView.ScaleType.FIT_XY));

                //itemBanners.add(data.getString("idslider"));
                //item_banner item = new item_banner();
                // item.setIdslider(data.getString("idslider"));
                String id = data.getString("id");


            }

            bannerSlider.setBanners(banners);

            try {
                for (int i = 0; i < dataSlider.length(); i++) {
                    JSONObject dataArray = null;

                    dataArray = dataSlider.getJSONObject(i);
                    item_banner item = new item_banner();
                    item.setIdslider(dataArray.getString("id"));
                    item.setGambar(dataArray.getString("pict"));
                    ItemBanner.add(item);
                }
            }catch (JSONException e){

            }


            bannerSlider.setOnBannerClickListener(new OnBannerClickListener() {
                @Override
                public void onClick(int position) {
                    //ke halaman view detail sliderju
                    String idbanner = ItemBanner.get(position).getIdslider();
                    String gambar = ItemBanner.get(position).getGambar();

                    /*Intent next = new Intent(getActivity(),BannerDetailActivity.class);
                    next.putExtra("gambar",gambar );
                    next.putExtra("idslider", idbanner);
                    startActivity(next);*/

                    //Toast.makeText(getContext(), "posisi : "+idbanner, Toast.LENGTH_LONG).show();

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
