package com.dinustek.pdamsemarang.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.controller.AppConfig;
import com.dinustek.pdamsemarang.controller.AppController;
import com.dinustek.pdamsemarang.helper.UserHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegistrasiActivity extends AppCompatActivity {
    EditText edNoPel, edEmail,edNama, edPassword;
    CheckBox checkBox;
    String NoPel, Email, Nama, Password;
    ProgressDialog pDialog;
    String TAG = "Regis";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);
        getSupportActionBar().setTitle("Buat Akun");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pDialog = new ProgressDialog(this);

        InitViews();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //app icon in action bar clicked
                this.finish();
                return true;
            default:

        }
        return super.onOptionsItemSelected(item);
    }
    private void InitViews(){
        edNoPel = (EditText)findViewById(R.id.Reg_No_Pelanggan);
        edEmail = (EditText)findViewById(R.id.Reg_Email);
        edNama = findViewById(R.id.Reg_Nama);
        edPassword = findViewById(R.id.Reg_Password);

        checkBox = (CheckBox) findViewById(R.id.Reg_Check);
    }
    private void getData(){
        NoPel = edNoPel.getText().toString();
        Email = edEmail.getText().toString();
        Nama = edNama.getText().toString();
        Password = edPassword.getText().toString();
    }
    public void goRegis(View view){
        getData();
        if (isNopelValid(NoPel) && isEmailValid(Email) && isCheckValid()){
            //Regis
            Req_Regis(NoPel, Email, Password, Nama);
        }else {
            Toast.makeText(this, "Silahkan Lengkapi Data", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void Req_Regis(final String nopel, final String email, final String password, final String nama){
        showPDialog("Loading ...");
        // Creating volley request obj
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_Registrasi, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                // Parsing json
                try {
                    JSONObject jObj = new JSONObject(response);
                    String KodeRespon = jObj.getString("response");
                    if (KodeRespon.equals("00")){
                        //JSONArray data = jObj.getJSONArray("data");
                       // PasangData(data);
                        DialogPesan("Sukses", "Registrasi Berhasil, Silakan Cek Email Anda Untuk Kode Verifikasi");


                    }else {
                        Toast.makeText(getApplicationContext(),"error" , Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                finish();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("api_key",AppConfig.Apikey);
                params.put("nolangg", nopel);
                params.put("name", nama);
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 10, 1.0f));
        AppController.getInstance().addToRequestQueue(strReq);
    }
    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //finish();
                        Intent next = new Intent(getApplicationContext(), InputKodeVerifActivity.class);
                        startActivity(next);
                        finish();
                    }
                })
                .show();
    }
    private boolean isNopelValid(String s){
        if (!s.isEmpty()){
            return true;
        }else {
            return false;
        }
    }
    private boolean isEmailValid(String s){
        if (!s.isEmpty()){
            return true;
        }else {
            return false;
        }
    }
    private boolean isCheckValid(){
        if (checkBox.isChecked()){
            return true;
        }else {
            return false;
        }
    }
}
