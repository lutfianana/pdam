package com.dinustek.pdamsemarang.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.controller.AppConfig;
import com.dinustek.pdamsemarang.controller.AppController;
import com.dinustek.pdamsemarang.helper.SessionManager;
import com.dinustek.pdamsemarang.helper.UserHelper;
import com.dinustek.pdamsemarang.helper.helper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DetailTagihanActivity extends AppCompatActivity {
    ProgressDialog pDialog;
    //LinearLayout TagBayar, DT_RL_Nama, DT_RL_Alamat;
    LinearLayout tr_Nama, tr_Alamat, tr_TempatBayar, tr_TanggalBayar;
    boolean Bayar = false;
    String TAG = "Detail Tagihan";
    String Tahun, Bulan, No_Langg;
    ImageView imageView;
    LinearLayout Parent, NeedLoginFull;
    TextView tvNoPelanggan, tvNama, tvAlamat, tvCabang, tvBulanRekening, tvPemakaian, tvTotalTagihan, tvStatusBayar, tvTanggalBayar, tgTempatPembayaran;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tagihan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Tagihan");
        session = new SessionManager(getApplicationContext());

        InitViews();
        Tahun = getIntent().getStringExtra("tahun");
        Bulan = getIntent().getStringExtra("bulan");
        if (!getIntent().getStringExtra("nopel").equals("")){
            No_Langg = getIntent().getStringExtra("nopel");
            Req_DetailTagihanPublik(Tahun, Bulan, No_Langg);
            imageView.setVisibility(View.GONE);
            tr_Nama.setVisibility(View.GONE);
            tr_Alamat.setVisibility(View.GONE);
        }else {
            Req_DetailTagihan(Tahun, Bulan);
            Req_FotoMeterTagihan(Tahun, Bulan);
        }

        //Bayar = getIntent().getBooleanExtra("bayar", false);

    }
    private void InitViews(){
        pDialog = new ProgressDialog(this);
        Parent = (LinearLayout)findViewById(R.id.DT_Parent);
        Parent.setVisibility(View.GONE);
        NeedLoginFull = (LinearLayout)findViewById(R.id.DT_NeedLoginFull);
        NeedLoginFull.setVisibility(View.GONE);
        imageView = (ImageView)findViewById(R.id.DT_FotoMeter);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenImageViewer(((BitmapDrawable)imageView.getDrawable()).getBitmap());
            }
        });
        tvNoPelanggan = (TextView)findViewById(R.id.DT_NoPelanggan);
        tvNama = (TextView)findViewById(R.id.DT_Nama);
        tvAlamat = (TextView)findViewById(R.id.DT_Alamat);
        tvCabang = (TextView)findViewById(R.id.DT_Cabang);
        tvBulanRekening = (TextView)findViewById(R.id.DT_BulanRekening);
        tvPemakaian = (TextView)findViewById(R.id.DT_Pemakaian);
        tvTotalTagihan = (TextView)findViewById(R.id.DT_TotalTagihan);
        tvStatusBayar = (TextView)findViewById(R.id.DT_StatusBayar);
        tvTanggalBayar = (TextView)findViewById(R.id.DT_TglBayar);
        tgTempatPembayaran = (TextView)findViewById(R.id.DT_TempatBayar);

        tr_Nama = (LinearLayout)findViewById(R.id.DT_tr_Nama);
        tr_Alamat = (LinearLayout)findViewById(R.id.DT_tr_Alamat);
        tr_TanggalBayar = (LinearLayout)findViewById(R.id.DT_tr_TanggalBayar);
        tr_TempatBayar = (LinearLayout)findViewById(R.id.DT_tr_TempatBayar);

//        TagBayar = (LinearLayout)findViewById(R.id.TagBayar);
//        DT_RL_Nama = (LinearLayout)findViewById(R.id.DT_RL_Nama);
//        DT_RL_Alamat = (LinearLayout)findViewById(R.id.DT_RL_Alamat);

    }
    private void PasangData(JSONObject data){
        try {
            tvNoPelanggan.setText(data.getString("NOLANGG"));
            tvNama.setText(UserHelper.Nama);
            tvAlamat.setText(UserHelper.Alamat);
            tvCabang.setText(helper.getCabang(data.getInt("CABANG")));
            tvBulanRekening.setText(helper.getBulanRekening(data.getString("PERIODE")));
            tvPemakaian.setText(data.getString("M3")+" M3");
            tvTotalTagihan.setText(helper.getRupiah(data.getInt("TOTALBYR")));
            tvStatusBayar.setText(helper.getStatusBayar(data.getString("TGIBAYAR")));
            tvTanggalBayar.setText(data.getString("TGIBAYAR"));
            tgTempatPembayaran.setText(data.getString("NAMAKAS"));

            if (data.getString("TGIBAYAR").equals("") || data.getString("TGIBAYAR").equals("null")){
                tr_TanggalBayar.setVisibility(View.GONE);
                tr_TempatBayar.setVisibility(View.GONE);
                ShowHideLogin();
            }else {
                tr_TanggalBayar.setVisibility(View.VISIBLE);
                tr_TempatBayar.setVisibility(View.VISIBLE);
                ShowHideLogin();
            }
            Parent.setVisibility(View.VISIBLE);
        }catch (JSONException e){

        }
    }
    private void ShowHideLogin(){
        if (!session.isLoggedIn()){
            NeedLoginFull.setVisibility(View.VISIBLE);
        }
    }
    public void OpenLoginFromDetail(View view){
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }
    private void OpenImageViewer(Bitmap bitmap){
        UserHelper.bitmapImage = bitmap;
        Intent i = new Intent(this, ImagePreviewerActivity.class);
        startActivity(i);
    }
    private void Base64ToImage(String StrImg){
        byte[] imageBytes = Base64.decode(StrImg, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        imageView.setImageBitmap(decodedImage);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //app icon in action bar clicked
                this.finish();
                return true;
            default:

        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void Req_DetailTagihan(final String tahun, final String bulan){
        showPDialog("Loading ...");
        // Creating volley request obj
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_TagihanBulan, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                // Parsing json
                try {
                    JSONObject jObj = new JSONObject(response);
                    Boolean error = jObj.getBoolean("error");

                    if(!error){
                        JSONObject data = jObj.getJSONObject("data");
                        PasangData(data);

                        //DialogLoginPesan("Perhatian", "Login berhasil");
                    }else{
                        Toast.makeText(getApplicationContext(), jObj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                finish();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("token", UserHelper.UToken);
                params.put("tahun", tahun);
                params.put("bulan", bulan);
                return params;
            }

        };

        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 10, 1.0f));
        AppController.getInstance().addToRequestQueue(strReq);
    }
    private void Req_DetailTagihanPublik(final String tahun, final String bulan, final String no_pelanggan){
        showPDialog("Loading ...");
        // Creating volley request obj
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_TagihanPublik, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                // Parsing json
                try {
                    JSONObject jObj = new JSONObject(response);
                    Boolean error = jObj.getBoolean("error");

                    if(!error){
                        JSONObject data = jObj.getJSONObject("data");
                        PasangData(data);

                        //DialogLoginPesan("Perhatian", "Login berhasil");
                    }else{
                        Toast.makeText(getApplicationContext(), jObj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                finish();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("nolangg", No_Langg);
                params.put("tahun", tahun);
                params.put("bulan", bulan);
                return params;
            }

        };

        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 10, 1.0f));
        AppController.getInstance().addToRequestQueue(strReq);
    }
    private void Req_FotoMeterTagihan(final String tahun, final String bulan){
        showPDialog("Loading ...");
        // Creating volley request obj
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_MeterBulanan, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                // Parsing json
                try {
                    JSONObject jObj = new JSONObject(response);
                    Boolean error = jObj.getBoolean("error");

                    if(!error){
                        JSONObject data = jObj.getJSONObject("data");
                        Base64ToImage(data.getString("gambar"));

                        //DialogLoginPesan("Perhatian", "Login berhasil");
                    }else{
                        Toast.makeText(getApplicationContext(), jObj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                finish();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("token", UserHelper.UToken);
                params.put("tahun", tahun);
                params.put("bulan", bulan);
                return params;
            }

        };

        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 10, 1.0f));
        AppController.getInstance().addToRequestQueue(strReq);
    }
}
