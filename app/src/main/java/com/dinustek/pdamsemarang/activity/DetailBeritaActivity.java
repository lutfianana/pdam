package com.dinustek.pdamsemarang.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.dinustek.pdamsemarang.R;

public class DetailBeritaActivity extends AppCompatActivity {
    TextView tvJudul, tvTanggal, tvKonten;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_berita);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Berita");

        InitViews();
        PasangBerita();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //app icon in action bar clicked
                this.finish();
                return true;
            default:

        }
        return super.onOptionsItemSelected(item);
    }
    private void InitViews(){
        tvJudul = (TextView)findViewById(R.id.DB_Judul);
        tvTanggal = (TextView)findViewById(R.id.DB_Tanggal);
        tvKonten = (TextView)findViewById(R.id.DB_Konten);
    }
    private void PasangBerita(){
        if (getIntent()!=null){
            tvJudul.setText(getIntent().getStringExtra("db_judul"));
            tvTanggal.setText(getIntent().getStringExtra("db_tanggal"));
            tvKonten.setText(getIntent().getStringExtra("db_konten"));
        }
    }
}
