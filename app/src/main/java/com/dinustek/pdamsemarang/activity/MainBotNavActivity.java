package com.dinustek.pdamsemarang.activity;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.fragment.BerandaFragment;
import com.dinustek.pdamsemarang.fragmentpengaduan.PengaduanFragment;
import com.dinustek.pdamsemarang.fragmenttagihan.TagihanFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainBotNavActivity extends AppCompatActivity {
    Fragment fragment = null;
    FragmentTransaction fragmentTransaction;
    TextView mTitleTextView, mSubTitleTextView;
    Typeface tipefont;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_beranda:
                    fragment = new BerandaFragment();
                    setFragment();
                    return true;
                case R.id.navigation_tagihan:
                    fragment = new TagihanFragment();
                    setFragment();
                    return true;
                case R.id.navigation_pengaduan:
                    fragment = new PengaduanFragment();
                    setFragment();
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_bot_nav);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Raleway-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
                    );

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        tipefont = Typeface.createFromAsset(getApplication().getAssets(), "fonts/RobotoCondensed-Regular.ttf");


        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
        mSubTitleTextView = (TextView) mCustomView.findViewById(R.id.subtitle_text);

        mSubTitleTextView.setTypeface(tipefont);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        setFragment();
    }
    public void Keluar(View view){
        finish();
    }
    public void dialogMultiAkun(View view){
        new MaterialDialog.Builder(this)
                .title("Pilih Akun")
                .items(R.array.akun)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int position, CharSequence text) {
                        /**
                         * If you use alwaysCallSingleChoiceCallback(), which is discussed below,
                         * returning false here won't allow the newly selected radio button to actually be selected.
                         **/
                        mTitleTextView.setText(text);
                        //Toast.makeText(getApplicationContext(), position+" - "+text, Toast.LENGTH_LONG).show();
                        return true;
                    }
                })
                .positiveText(R.string.pilih)
                .show();
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    public void setFragment(){

        if(fragment == null ){
            fragment = new BerandaFragment();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_navbot, fragment, null);
            fragmentTransaction.commit();
        }
        else if (fragment != null) {
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_navbot, fragment, null);
            fragmentTransaction.commit();
        }
    }
}
