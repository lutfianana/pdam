package com.dinustek.pdamsemarang.activity;

import android.app.ProgressDialog;
import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.controller.AppConfig;
import com.dinustek.pdamsemarang.controller.AppController;
import com.dinustek.pdamsemarang.fcm.MyFirebaseInstanceIDService;
import com.dinustek.pdamsemarang.fragment.AkunFragment;
import com.dinustek.pdamsemarang.helper.RealmController;
import com.dinustek.pdamsemarang.helper.SessionManager;
import com.dinustek.pdamsemarang.helper.UserHelper;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    ProgressDialog pDialog;
    String TAG = "LoginActivity";
    EditText edUsername;
    TextInputEditText edPassword;
    RealmController RC;
    SessionManager session;
    RelativeLayout Log_RL_Skip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        getSupportActionBar().setTitle("Login");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pDialog = new ProgressDialog(this);

        RC = new RealmController(getApplication());
        session = new SessionManager(getApplicationContext());
        Initial();
        SaveToken();
        //Logout();
    }
    private void Initial(){
        Log_RL_Skip = (RelativeLayout)findViewById(R.id.Log_RL_Skip);
        Log_RL_Skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), TampungActivity.class);
                i.putExtra("page", 4);
                startActivity(i);
            }
        });
        edPassword = (TextInputEditText)findViewById(R.id.Log_Password);
        edUsername = (EditText)findViewById(R.id.Log_Username);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //app icon in action bar clicked
                this.finish();
                return true;
            default:

        }
        return super.onOptionsItemSelected(item);
    }
    public void btnBatal(View view){
        finish();
    }
    public void btnRegisKliked(View view){
        Intent i = new Intent(this, RegistrasiActivity.class);
        startActivity(i);
        finish();
    }
    public void btnLoginKliked(View view){
        if (isPasswordValid() && isUsernameValid()){
            Req_Login(edUsername.getText().toString(), edPassword.getText().toString());
        }else {
            DialogPesan("Gagal Login", "Silahkan periksa data login anda");
        }
    }
    private boolean isUsernameValid(){
        if (edUsername.getText().toString().equals("")){
            return false;
        }else {
            return true;
        }
    }
    private boolean isPasswordValid(){
        if (edPassword.getText().toString().equals("")){
            return false;
        }else {
            return true;
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void Req_Login(final String username, final String password){
        showPDialog("Loading ...");
        // Creating volley request obj
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_Login, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                // Parsing json
                try {
                    JSONObject jObj = new JSONObject(response);
                    Boolean error = jObj.getBoolean("error");

                    if(!error){
                        UserHelper.UToken = jObj.getString("token");
                        Req_Profile();
                        GotoMain();
                        TampungActivity.getInstance().CloseWhenLogin();
                        //DialogLoginPesan("Perhatian", "Login berhasil");
                    }else{
                        Toast.makeText(getApplicationContext(), jObj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                finish();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("username", username);
                params.put("password", password);
                return params;
            }

        };

        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 10, 1.0f));
        AppController.getInstance().addToRequestQueue(strReq);
    }
    private void Req_Profile(){
        showPDialog("Loading ...");
        // Creating volley request obj
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_Profil, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                // Parsing json
                try {
                    JSONObject jObj = new JSONObject(response);
                    Boolean error = jObj.getBoolean("error");
                    //boolean status = jObj.getBoolean("status");

                    if(!error){
                        JSONObject data = jObj.getJSONObject("data");

                        UserHelper.Nama = data.getString("NAMA");
                        UserHelper.Alamat = data.getString("ALAMAT");
                        UserHelper.NoPelanggan = data.getString("NOLANGG");
                        UserHelper.NoMeter = data.getString("NOMORMETER");

                        session.setLogin(true);
                        RC.AddUsersData(UserHelper.token_FCM, UserHelper.UToken, UserHelper.NoPelanggan, UserHelper.Nama, UserHelper.Alamat, UserHelper.NoMeter);

                    }else{
                        Toast.makeText(getApplicationContext(), jObj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                finish();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("token", UserHelper.UToken);

                return params;
            }

        };

        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 10, 1.0f));
        AppController.getInstance().addToRequestQueue(strReq);
    }
    private void GotoMain(){
        Intent i = new Intent(getApplicationContext(), MainTabActivity.class);
        startActivity(i);
        finish();
    }
    private void DialogLoginPesan(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent i = new Intent(getApplicationContext(), MainTabActivity.class);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }
    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .show();
    }
    private void SaveToken(){
        if (session.isTokenIn()!=null){
            UserHelper.token_FCM = session.isTokenIn();
            Log.d("Token Tersimpan", UserHelper.token_FCM);
            //Toast.makeText(this, "Token Siap", Toast.LENGTH_SHORT).show();
        }else if (UserHelper.token_FCM!=null){
            session.setToken(UserHelper.token_FCM);
            Log.d("Token di simpan ke pref", UserHelper.token_FCM);
            //Toast.makeText(this, "Token Siap", Toast.LENGTH_SHORT).show();
        }else {
            //AmbilData();
            Log.d("Token null", "");

            // TODO FCM prepare
            MyFirebaseInstanceIDService mFS = new MyFirebaseInstanceIDService();
            mFS.getTokenID();

            if (UserHelper.token_FCM!=null || !UserHelper.token_FCM.equals(null) || !UserHelper.token_FCM.isEmpty()){
                Log.d("get New Token", UserHelper.token_FCM);
            }else{
                Log.d("get New Token", "Failed get new Token");
            }

            //Toast.makeText(this, "Token Else : "+UserHelper.token_FCM, Toast.LENGTH_SHORT).show();
            SaveToken();
        }
    }

}
