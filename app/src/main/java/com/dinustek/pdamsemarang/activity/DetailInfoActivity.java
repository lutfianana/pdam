package com.dinustek.pdamsemarang.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.dinustek.pdamsemarang.R;

public class DetailInfoActivity extends AppCompatActivity {
    TextView tv_Title, tv_Text;
    private WebView webView;
    private String TAG = "TampungWebview";
    String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Informasi Pelanggan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tv_Text = (TextView)findViewById(R.id.tv_text);
        tv_Title = (TextView)findViewById(R.id.tv_title);
        /*
        if (getIntent().getStringExtra("jenis").equals("ganti")){
            tv_Title.setText(R.string.p_ganti);
            tv_Text.setText(R.string.ganti);
        }
        */
        url = getIntent().getStringExtra("url");

        webView = (WebView)findViewById(R.id.webview_tampung);

        goHalaman();
    }
    private void goHalaman(){
        webView.loadUrl(url);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //app icon in action bar clicked
                this.finish();
                return true;
            default:

        }
        return super.onOptionsItemSelected(item);
    }
}
