package com.dinustek.pdamsemarang.activity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.fragment.BerandaFragment;
import com.dinustek.pdamsemarang.fragment.BeritaFragment;
import com.dinustek.pdamsemarang.fragment.KontakFragment;
import com.dinustek.pdamsemarang.fragmentpengaduan.ParentPengaduanFragment;
import com.dinustek.pdamsemarang.fragmentpengaduan.PengaduanFragment;
import com.dinustek.pdamsemarang.fragmentsupport.CekTagihanFragment;
import com.dinustek.pdamsemarang.fragmentsupport.GangguanAirFragment;
import com.dinustek.pdamsemarang.fragmentsupport.NeedLoginFragment;
import com.dinustek.pdamsemarang.fragmenttagihan.TagihanFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TampungActivity extends AppCompatActivity {
    Fragment fragment = null;
    FragmentTransaction fragmentTransaction;
    int page = 1;
    static TampungActivity tampungActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampung);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tampungActivity = this;

        page = getIntent().getIntExtra("page", 1);

        setViewFragment(page);

    }
    public static TampungActivity getInstance(){
        return tampungActivity;
    }
    public void CloseWhenLogin(){
        finish();
        Log.d("Tampung Keluar", "CloseWhenLogin: Keluar");
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //app icon in action bar clicked
                this.finish();
                return true;
            default:

        }
        return super.onOptionsItemSelected(item);
    }
    private void setViewFragment(int halaman){
        switch (halaman){
            case 1:
                HideHeader(false);
                setJudul("Tagihan");
                fragment = new TagihanFragment();
                setFragment();
                break;
            case 2:
                HideHeader(false);
                setJudul("Pengaduan");
                fragment = new ParentPengaduanFragment();
                setFragment();
                break;
            case 3:
                HideHeader(false);
                setJudul("Gangguan Air");
                fragment = new GangguanAirFragment();
                setFragment();
                break;
            case 4:
                HideHeader(true);
                setJudul("Cek Tagihan");
                fragment = new CekTagihanFragment();
                setFragment();
                break;
            case 5:
                HideHeader(false);
                setJudul("Berita");
                fragment = new BeritaFragment();
                setFragment();
                break;
            case 6:
                HideHeader(false);
                setJudul("Kontak");
                fragment = new KontakFragment();
                setFragment();
                break;
            case 7:
                HideHeader(false);
                setJudul("Pengaduan");
                fragment = new NeedLoginFragment();
                setFragment();
                break;
        }
    }
    private void HideHeader(boolean hide){
        if (hide){
            getSupportActionBar().hide();
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }else {
            getSupportActionBar().show();
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

    }
    private void  setJudul(String title){
        getSupportActionBar().setTitle(title);
    }
    private void  setHeader(String title, String subtitle){
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setSubtitle(subtitle);
    }
    public void setFragment(){

        if(fragment == null ){
            fragment = new BerandaFragment();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.tampung_frame, fragment, null);
            fragmentTransaction.commit();
        }
        else if (fragment != null) {
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.tampung_frame, fragment, null);
            fragmentTransaction.commit();
        }
    }
}
