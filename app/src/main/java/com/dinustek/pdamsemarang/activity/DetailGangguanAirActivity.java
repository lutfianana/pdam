package com.dinustek.pdamsemarang.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.dinustek.pdamsemarang.R;

public class DetailGangguanAirActivity extends AppCompatActivity {
    TextView tvJudul, tvKonten, tvLokasi, tvTanggal;
    String Judul, Konten, Lokasi, Tanggal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_gangguan_air);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Informasi Gangguan");
        InitViews();

        if (getIntent()!=null){
            Judul = getIntent().getStringExtra("dga_judul");
            Konten = getIntent().getStringExtra("dga_konten");
            Lokasi = getIntent().getStringExtra("dga_lokasi");
            Tanggal = getIntent().getStringExtra("dga_tanggal");
            PasangData();
        }else {
            Toast.makeText(this, "Gagal membuka detail", Toast.LENGTH_LONG).show();
            finish();
        }
    }
    public void Bagikan(View view){
        ShareAction(Judul, Konten, Lokasi, Tanggal);
    }
    private void ShareAction(String judul, String konten, String lokasi, String tanggal){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Android PDAM Kota Semarang\n"+judul+"\n"+konten+"\n"+lokasi+"\n"+tanggal;
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, judul);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Bagikan dengan"));
    }
    private void InitViews(){
        tvJudul = (TextView)findViewById(R.id.DGA_Judul);
        tvKonten = (TextView)findViewById(R.id.DGA_Konten);
        tvLokasi = (TextView)findViewById(R.id.DGA_Lokasi);
        tvTanggal = (TextView)findViewById(R.id.DGA_Tanggal);
    }
    private void PasangData(){
        tvJudul.setText(Judul);
        tvKonten.setText(Konten);
        tvLokasi.setText(Lokasi);
        tvTanggal.setText(Tanggal);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //app icon in action bar clicked
                this.finish();
                return true;
            default:

        }
        return super.onOptionsItemSelected(item);
    }
}

