package com.dinustek.pdamsemarang.activity;

import android.graphics.drawable.ColorDrawable;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.helper.UserHelper;
import com.jsibbold.zoomage.ZoomageView;

public class ImagePreviewerActivity extends AppCompatActivity {
    ZoomageView zImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_previewer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.black)));
        getSupportActionBar().setTitle("Photo Viewer");
        zImage = (ZoomageView)findViewById(R.id.Photo_Image);
        if (hasValidBitmap()){
            zImage.setImageBitmap(UserHelper.bitmapImage);
        }else{
            Toast.makeText(this, "Gagal Load Gambar", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
    private synchronized boolean hasValidBitmap() {
        return UserHelper.bitmapImage != null && !UserHelper.bitmapImage.isRecycled();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
