package com.dinustek.pdamsemarang.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.fragment.AkunFragment;
import com.dinustek.pdamsemarang.fragment.BerandaFragment;
import com.dinustek.pdamsemarang.fragment.BeritaFragment;
import com.dinustek.pdamsemarang.fragment.InfoPelangganFragment;
import com.dinustek.pdamsemarang.fragment.KontakFragment;
import com.dinustek.pdamsemarang.fragmentpengaduan.PengaduanFragment;
import com.dinustek.pdamsemarang.helper.RealmController;
import com.dinustek.pdamsemarang.helper.SessionManager;
import com.dinustek.pdamsemarang.helper.UserHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainTabActivity extends AppCompatActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    TextView mTitleTextView, mSubTitleTextView;
    Typeface tipefont;
    View mCustomView;
    ActionBar mActionBar;

    private int[] imageResId = new int[] {
            R.drawable.ic_beranda,
            R.drawable.ic_info_pelanggan,
            R.drawable.ic_berita,
            R.drawable.ic_kontak,
            R.drawable.ic_akun
    };
    RealmController RC;
    SessionManager session;

    String TAG = "PDAM Semarang";
    //Set For GPS
    protected GoogleApiClient mGoogleApiClient;

    int currentapiVersion;
    int time_request_GPS = 30;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    LocationManager locationManager;
    Location mCurlocation;

    private static final int REQUEST_CHECK_SETTINGS = 1;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;

    static MainTabActivity mainTabActivity;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tab);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mainTabActivity = this;

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Raleway-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        RC = new RealmController(getApplication());
        session = new SessionManager(getApplicationContext());


        TampilHeader(true);

        tipefont = Typeface.createFromAsset(getApplication().getAssets(), "fonts/RobotoCondensed-Regular.ttf");

        viewPager = (ViewPager) findViewById(R.id.viewpager_main);
        setupViewPager(viewPager);
        //disable swaping layout
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                viewPager.setCurrentItem(viewPager.getCurrentItem());
                return true;
            }
        });

        tabLayout = (TabLayout) findViewById(R.id.tabs_main);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(imageResId[0]);
        tabLayout.getTabAt(1).setIcon(imageResId[1]);
        tabLayout.getTabAt(2).setIcon(imageResId[2]);
        tabLayout.getTabAt(3).setIcon(imageResId[3]);
        tabLayout.getTabAt(4).setIcon(imageResId[4]);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark) , PorterDuff.Mode.SRC_IN);
                switch(tab.getPosition()) {
                    case 0:
                        tab.getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark) , PorterDuff.Mode.SRC_IN);
                        viewPager.setCurrentItem(0);
                        setTitlePage("Beranda");
                        TampilHeader(true);
                        PasangHeaderData();
                        break;
                    case 1:
                        viewPager.setCurrentItem(1);
                        setTitlePage("Informasi Pelanggan");
                        TampilHeader(false);
                        break;
                    case 2:
                        viewPager.setCurrentItem(2);
                        setTitlePage("Berita");
                        TampilHeader(false);
                        break;
                    case 3:
                        viewPager.setCurrentItem(3);
                        setTitlePage("Kontak");
                        TampilHeader(false);
                        break;
                    case 4:
                        viewPager.setCurrentItem(4);
                        setTitlePage("Akun");
                        TampilHeader(false);
                        break;
                    default:
                        viewPager.setCurrentItem(0);
                        setTitlePage("Beranda");
                        tab.getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark) , PorterDuff.Mode.SRC_IN);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.footer_tidak_aktif) , PorterDuff.Mode.SRC_ATOP);
                //tab.getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.footer_tidak_aktif) , PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark) , PorterDuff.Mode.SRC_IN);
            }
        });

        getAndroidVersion();
        buildGoogleApiClient();//maps google location

        checkPermissions();
        cekGPSandProvider();
        //PasangHeaderData();

    }
    private void DialogLogout(){
        new MaterialDialog.Builder(this)
                .title("Konfirmasi Keluar")
                .content("Apakah anda yakin akan keluar dari aplikasi?")
                .positiveText("YA")
                .negativeText("Tidak")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Logout();
                    }
                })
                .show();
    }

    //Set For GPS
    private void getLocFromGoogle(){
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mCurlocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mCurlocation != null) {
            setMyLocation(mCurlocation);
        } else {
            //Toast.makeText(this, "tes", Toast.LENGTH_LONG).show();
        }
    }
    private void cekGPSandProvider() {
        getGPS();
        //Toast.makeText(getApplicationContext(), "GPS : "+gps_enabled+"\nProvider : "+network_enabled, Toast.LENGTH_SHORT).show();
        if (gps_enabled == true || network_enabled == true) {
            //return true;
            setMyLocation(mCurlocation);
            //getLocFromGoogle();
            //GPS_Aktif = true;
        } else {
            getLocFromGoogle();
            //openSettingSet();
            // komen setting gps
            //return false;
        }
    }
    private void openSettingSet() {
        //if(cekGPSandProvider()==false) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder
                .setTitle("Perhatian")
                .setMessage("Your GPS is Off")
                .setPositiveButton("Setting", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                        //Toast.makeText(getBaseContext(), "Edited", Toast.LENGTH_SHORT).show();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openSettingSet();
                    }
                })
                .show();
//            getGPS();
        //}
    }
    //init gps
    private void getGPS() {
        // Get the LocationManager object from the System Service LOCATION_SERVICE
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        // Create a criteria object needed to retrieve the provider
        Criteria criteria = new Criteria();

        // Get the name of the best available provider
        String provider = locationManager.getBestProvider(criteria, true);

        // We can use the provider immediately to get the last known location
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;

        }


        // request that the provider send this activity GPS updates every time_request_GPS=30 seconds
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, time_request_GPS*1000, 0, this);
        } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, time_request_GPS*1000, 0, this);
        }

        mCurlocation = locationManager.getLastKnownLocation(provider);

        //cekGPSandProvider();
        setMyLocation(mCurlocation);


    }
    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }
    //get Android version
    private void getAndroidVersion(){
        currentapiVersion = android.os.Build.VERSION.SDK_INT;
        Log.d(TAG, "Android Version : "+currentapiVersion);

    }
    //set location
    private void setMyLocation(Location location) {
        //getGPS();
        if (location != null) {
            UserHelper.GPS_Loc_Lat = String.valueOf(location.getLatitude());
            UserHelper.GPS_Loc_Lang = String.valueOf(location.getLongitude());
        } else {
            UserHelper.GPS_Loc_Lat = null;
            UserHelper.GPS_Loc_Lang = null;
        }

        Log.d(TAG, "Lokasi Saat Ini : "+UserHelper.GPS_Loc_Lat + " dan " + UserHelper.GPS_Loc_Lang);
        //Toast.makeText(this, ""+UserHelper.Loc_Lat + " dan " + UserHelper.Loc_Lang, Toast.LENGTH_SHORT).show();
    }
    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else
                showSettingDialog();
        } else
            showSettingDialog();

    }
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }
    private void showSettingDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.

                        //DialogAlert("GPS is Enabled in your device");

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MainTabActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        Log.d("Settings", "GPS Result OK");
                        //DialogAlert("GPS is Enabled in your device");
                        buildGoogleApiClient();
                        //startLocationUpdates();
                        break;
                    case RESULT_CANCELED:
                        Log.d("Settings", "GPS Result Cancel");
                        DialogAlert("Aplikasi ini membutuhkan akses GPS. Mohon Aktifkan GPS. Terimakasih");
                        break;
                }
                break;
        }
    }
    //Hasil request permission
    /* On Request permission method to check the permisison is granted or not for Marshmallow+ Devices  */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_INTENT_ID: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //If permission granted show location dialog if APIClient is not null
                    if (mGoogleApiClient == null) {
                        buildGoogleApiClient();
                        showSettingDialog();
                    } else
                        showSettingDialog();


                } else {
                    DialogAlert("Aplikasi ini membutuhkan akses GPS. Mohon Aktifkan GPS. Terimakasih");
                    //Toast.makeText(MainBotActivity.this, "Location Permission denied.", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }
    private void responDialog(String message){
        new MaterialDialog.Builder(this)
                .title("PERHATIAN")
                .content(message)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                })
                .show();
    }
    private void DialogAlert(String message){
        new MaterialDialog.Builder(this)
                .title("PERHATIAN")
                .content(message)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        checkPermissions();
                    }
                })
                .show();
    }
    //start GoogleApiClient.ConnectionCallbacks
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "Google Connected");
        getLocFromGoogle();
    }
    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }
    //end GoogleApiClient.ConnectionCallbacks
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Google Connection Failed");
    }
    //start Location Listener
    @Override
    public void onLocationChanged(Location location) {
        if (mCurlocation!=location){
            setMyLocation(location);
        }else {
            setMyLocation(mCurlocation);
        }
    }
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }
    @Override
    public void onProviderEnabled(String s) {

    }
    @Override
    public void onProviderDisabled(String s) {

    }
    //end Location Listener
    protected void onStart() {
        Log.i(TAG, "Start Apps");
        mGoogleApiClient.connect();
        super.onStart();
    }
    protected void onStop() {
        Log.i(TAG, "Stop Apps");
        mGoogleApiClient.disconnect();
        super.onStop();
    }
    private void IconTabAktif(){

    }
    private void PasangHeaderData(){
        mTitleTextView.setText(UserHelper.Nama);
        mSubTitleTextView.setText(UserHelper.NoPelanggan);
    }
    private void TampilHeader(boolean isTampil){
        mActionBar = getSupportActionBar();
        if (isTampil){
            mActionBar.setDisplayShowHomeEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(false);
            LayoutInflater mInflater = LayoutInflater.from(this);
            mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);

            mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
            mSubTitleTextView = (TextView) mCustomView.findViewById(R.id.subtitle_text);
            PasangHeaderData();

            mSubTitleTextView.setTypeface(tipefont);

            mActionBar.setCustomView(mCustomView);
            mActionBar.setDisplayShowCustomEnabled(true);
        }else{
            mActionBar.setDisplayShowCustomEnabled(false);
            mActionBar.setDisplayShowTitleEnabled(true);
        }
    }
    private void Logout(){
        session.setLogin(false);
        session.setToken(null);
        RC.clearAllData();

        UserHelper.UToken = "";//untuk menampilkan halaman masuk/regis di beranda setelah logout
        //isMainAktif = false;

        Intent i = new Intent(this, TampungActivity.class);
        i.putExtra("page", 4);
        startActivity(i);
        finish();
    }
    public void GoToTagihan(View view){
        Intent i = new Intent(this, TampungActivity.class);
        i.putExtra("page", 1);
        startActivity(i);
    }
    public void GoToPengaduan(View view){
        Intent i = new Intent(this, TampungActivity.class);
        i.putExtra("page", 2);
        startActivity(i);
    }

    public void Keluar(View view){
        //Logout();
        DialogLogout();
    }
    public void dialogMultiAkun(View view){
        new MaterialDialog.Builder(this)
                .title("Pilih Akun")
                .items(R.array.akun)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int position, CharSequence text) {
                        /**
                         * If you use alwaysCallSingleChoiceCallback(), which is discussed below,
                         * returning false here won't allow the newly selected radio button to actually be selected.
                         **/
                        mTitleTextView.setText(text);
                        //Toast.makeText(getApplicationContext(), position+" - "+text, Toast.LENGTH_LONG).show();
                        return true;
                    }
                })
                .positiveText(R.string.pilih)
                .show();
    }
    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new BerandaFragment(), "Beranda");
        adapter.addFragment(new InfoPelangganFragment(), "Informasi Pelanggan");
        adapter.addFragment(new BeritaFragment(), "Berita");
        adapter.addFragment(new KontakFragment(), "Kontak");
        adapter.addFragment(new AkunFragment(), "Akun");
        viewPager.setAdapter(adapter);
    }
    public void setTitlePage(CharSequence title){
        getSupportActionBar().setTitle(title);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();



        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {

            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
//            return null;

//            Drawable image = ContextCompat.getDrawable(getApplicationContext(), imageResId[position]);
//            image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
//            // Replace blank spaces with image icon
//            SpannableString sb = new SpannableString("   " + mFragmentTitleList.get(position));
//            ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
//            sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//            return sb;

        }


    }
}
