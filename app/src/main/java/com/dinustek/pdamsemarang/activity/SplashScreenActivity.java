package com.dinustek.pdamsemarang.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.dinustek.pdamsemarang.BuildConfig;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.helper.RealmController;
import com.dinustek.pdamsemarang.helper.SessionManager;
import com.dinustek.pdamsemarang.helper.UserHelper;
import com.dinustek.pdamsemarang.model.Users;

public class SplashScreenActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 3;
    int versionCode = BuildConfig.VERSION_CODE;
    String versionName = BuildConfig.VERSION_NAME;
    ProgressDialog pDialog;
    String TAG ="Splash";
    Intent ie;

    RealmController RC;
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        RC = new RealmController(getApplication());
        session = new SessionManager(getApplicationContext());

        getSupportActionBar().hide();

        pDialog = new ProgressDialog(this);

        Delay();
    }
    private void CheckingIntentSession(){
        if (session.isLoggedIn()) {
            boolean isNull = false;
            // Fetching user details from sqlite
            Users user = RC.getFirstUser();
            try{
                UserHelper.NoPelanggan = user.getNo_Pelanggan();
                UserHelper.Alamat = user.getAlamat();
                UserHelper.NoMeter = user.getNo_Meter();
                UserHelper.Nama = user.getNama();
                UserHelper.token_FCM = user.getFcm_Token();
                UserHelper.UToken = user.getLogin_Token();

                ie = new Intent(SplashScreenActivity.this, MainTabActivity.class);
                startActivity(ie);
                finish();
            }catch (NullPointerException e){
                isNull = true;
                Log.e(TAG, "CheckingIntentSession: null data");
            }

            if (isNull){
                session.setLogin(false);
                session.setToken(null);

                Intent i = new Intent(this, SplashScreenActivity.class);
                startActivity(i);
                finish();
            }
        }else {
            ie = new Intent(SplashScreenActivity.this, TampungActivity.class);
            ie.putExtra("page", 4);
            startActivity(ie);
            finish();
        }

    }
    private void Delay(){   //Delay
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                CheckingIntentSession();

            }
        }, (SPLASH_DISPLAY_LENGTH*1000));
    }
}
