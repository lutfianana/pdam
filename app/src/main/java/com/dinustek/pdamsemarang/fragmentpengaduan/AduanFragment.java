package com.dinustek.pdamsemarang.fragmentpengaduan;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.adapter.adapter_aduanku;
import com.dinustek.pdamsemarang.controller.AppConfig;
import com.dinustek.pdamsemarang.controller.AppController;
import com.dinustek.pdamsemarang.helper.UserHelper;
import com.dinustek.pdamsemarang.helper.helper;
import com.dinustek.pdamsemarang.model.item_aduanku;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class AduanFragment extends Fragment {
    View rootView;
    ProgressDialog pDialog;
    String TAG = "Aduanku";

    //init recycler
    protected AduanFragment.LayoutManagerType mCurrentLayoutManagerType;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }
    private static final int SPAN_COUNT = 2;
    protected RecyclerView mRecyclerView;
    protected adapter_aduanku mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    private List<item_aduanku> Item_Data = new ArrayList<item_aduanku>();
    Bundle savedInstance;

    public AduanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialog = new ProgressDialog(getActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_aduan, container, false);
        InitRecycler();

        Req_ListPengaduan();

        return rootView;
    }
    private void InitRecycler(){
        //rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_aduanku);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mLayoutManager = new LinearLayoutManager(getActivity());

        mCurrentLayoutManagerType = AduanFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstance != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (AduanFragment.LayoutManagerType) savedInstance
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);

        mAdapter = new adapter_aduanku(getActivity(), Item_Data);
        // Set CustomAdapter as the adapter for RecyclerView.
        mRecyclerView.setAdapter(mAdapter);

    }
    private void PasangData(JSONArray item){
        try {
            Item_Data.clear();
            for (int i=0; i<item.length(); i++){
                JSONObject data = item.getJSONObject(i);

                item_aduanku items = new item_aduanku();

                if (!data.isNull("nama") || !data.getString("nama").equals("null")){
                    items.setNama(data.getString("nama"));
                    items.setTanggal(data.getString("tanggal")+"WIB");
                    items.setStatus(helper.getStatusAduan(data.getInt("status")));
                    items.setLaporan(data.getString("laporan"));
                    items.setFoto(data.getString("foto"));
                    items.setKategori(data.getString("kategori"));

                    Item_Data.add(items);
                }

            }
            mAdapter.notifyDataSetChanged();
        }catch (JSONException e){
            Toast.makeText(getActivity(), ""+e, Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void Req_ListPengaduan(){
        showPDialog("Loading ...");
        // Creating volley request obj
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ListPengaduan, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                // Parsing json
                try {
                    JSONObject jObj = new JSONObject(response);
                    Boolean error = jObj.getBoolean("error");

                    if(!error){
                        //JSONObject data = jObj.getJSONObject("data");
                        PasangData(jObj.getJSONArray("data"));

                        //DialogLoginPesan("Perhatian", "Login berhasil");
                    }else{
                        Toast.makeText(getActivity(), jObj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                finish();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("token", UserHelper.UToken);
                return params;
            }

        };

        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 10, 1.0f));
        AppController.getInstance().addToRequestQueue(strReq);
    }
    public void setRecyclerViewLayoutManager(AduanFragment.LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mCurrentLayoutManagerType = AduanFragment.LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = AduanFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            case STRAGGEREDGRID_LAYOUT_MANAGER:
                mLayoutManager = new StaggeredGridLayoutManager(SPAN_COUNT, StaggeredGridLayoutManager.VERTICAL);
                mCurrentLayoutManagerType = AduanFragment.LayoutManagerType.STRAGGEREDGRID_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = AduanFragment.LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }

}
