package com.dinustek.pdamsemarang.fragmentpengaduan;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.fragmenttagihan.TagihanFragment;
import com.dinustek.pdamsemarang.fragmenttagihan.TagihanTerbayarFragment;
import com.dinustek.pdamsemarang.fragmenttagihan.TagihanTunggakanFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParentPengaduanFragment extends Fragment {
    View rootView;
    private TabLayout tabLayout = null;
    private ViewPager viewPager = null;
    ImageView imgCC, imgWA;

    public ParentPengaduanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_parent_pengaduan, container, false);

        imgCC = rootView.findViewById(R.id.link_CC);
        imgWA = rootView.findViewById(R.id.link_WA);
        imgWA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //show
                GoToWA();
            }
        });

        imgCC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //GOTOCC
                GoToCC();
            }
        });



        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager_pengaduan);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs_pengaduan);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch(tab.getPosition()) {
                    case 0:
                        viewPager.setCurrentItem(0);
                        break;
                    case 1:
                        viewPager.setCurrentItem(1);
                        break;
                    default:
                        viewPager.setCurrentItem(0);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return rootView;

    }
    private void setupViewPager(ViewPager viewPager) {

        ParentPengaduanFragment.ViewPagerAdapter adapter = new ParentPengaduanFragment.ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new PengaduanFragment(), "Form Pengaduan");
        adapter.addFragment(new AduanFragment(), "Aduan");
        viewPager.setAdapter(adapter);
    }

    private void GoToWA(){
        //WA https://api.whatsapp.com/send?phone=6281348500060
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://api.whatsapp.com/send?phone=6281348500060"));
        startActivity(i);
    }
    private void GoToCC(){
        //WA https://api.whatsapp.com/send?phone=6281348500060
        String uri = "tel:02476920999";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(uri));
        startActivity(i);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();


        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {

            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
//            return null;

//            Drawable image = ContextCompat.getDrawable(getApplicationContext(), imageResId[position]);
//            image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
//            // Replace blank spaces with image icon
//            SpannableString sb = new SpannableString("   " + mFragmentTitleList.get(position));
//            ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
//            sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//            return sb;

        }
    }
}
