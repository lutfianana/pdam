package com.dinustek.pdamsemarang.fragmentpengaduan;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.controller.AppConfig;
import com.dinustek.pdamsemarang.controller.AppController;
import com.dinustek.pdamsemarang.helper.FileManager;
import com.dinustek.pdamsemarang.helper.Template;
import com.dinustek.pdamsemarang.helper.UserHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.zelory.compressor.Compressor;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class PengaduanFragment extends Fragment {
    View rootView;
    ImageView imageView;
    Button btnPilFoto, btnProses, btnBatal;
    TextView tvPilKat, tvPilSubKat, tvKatPil, tvSubKatPil, txt_LampirFoto;
    EditText edCatatan;
    ImageView imgCC, imgWA;
    RelativeLayout PilKat, PilSubKat;
    int JenisKategori = 3;
    ProgressDialog pDialog;
    String TAG = "Pengaduan";
    String Id_Kategori = "";
    LinearLayout LL_Peng_PilFot;

    private File compressedImageFile;
    boolean isSetImage = false;
    private static String[] CHOOSE_FILE = {"Camera", "File manager"};
    private List<String> PILIH_TEKNIS = new ArrayList<String>();
    private List<String> ID_TEKNIS = new ArrayList<String>();
    private List<String> PILIH_NON_TEKNIS = new ArrayList<String>();
    private List<String> ID_NON_TEKNIS = new ArrayList<String>();
    private Uri mOutputUri = null;
    private File mFile = null;
    private static final int STORAGE_PERMISSION_CODE = 123;
    private static final int MY_REQUEST_CODE = 111;
    private Bitmap bitmap=null;
    OkHttpClient client;
    RequestBody requestBody;
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
    private static final MediaType MEDIA_MULTIPART = MediaType.parse("multipart/form-data");

    public PengaduanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialog = new ProgressDialog(getActivity());
        client = new OkHttpClient();
        requestStoragePermission();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_pengaduan, container, false);

        InitView();
        Req_Kategori();


        return rootView;
    }
    private void InitView(){
        imgCC = rootView.findViewById(R.id.link_CC);
        imgWA = rootView.findViewById(R.id.link_WA);


        tvPilKat = (TextView)rootView.findViewById(R.id.tv_PilKat);
        tvPilSubKat = (TextView)rootView.findViewById(R.id.tv_PilSubKat);
        tvKatPil = (TextView)rootView.findViewById(R.id.tv_KatPil);
        tvSubKatPil = (TextView)rootView.findViewById(R.id.tv_SubKatPil);
        txt_LampirFoto = (TextView)rootView.findViewById(R.id.txt_LampirFoto);

        PilKat = (RelativeLayout)rootView.findViewById(R.id.Fr_Ad_Kategori);
        PilSubKat = (RelativeLayout)rootView.findViewById(R.id.Fr_Ad_SubKategori);
        PilKat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogKategori();
            }
        });
        PilSubKat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (JenisKategori==0){
                    dialogSubTeknis();
                }else if (JenisKategori==1){
                    dialogSubNonTeknis();
                }else {
                    Toast.makeText(getActivity(), "Silahkan Pilih Kategori terlebih dahulu", Toast.LENGTH_SHORT).show();
                }

            }
        });
        imageView = (ImageView)rootView.findViewById(R.id.P_imageView);
        btnPilFoto =  (Button)rootView.findViewById(R.id.P_btnPilFoto);
        btnPilFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFoto();
            }
        });
        LL_Peng_PilFot = (LinearLayout)rootView.findViewById(R.id.LL_Peng_PilFot);
        LL_Peng_PilFot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogFoto();
            }
        });
        btnProses =  (Button)rootView.findViewById(R.id.P_btnProses);
        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CekingForUpload();
            }
        });
        btnBatal =  (Button)rootView.findViewById(R.id.P_btnBatal);
        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        edCatatan = (EditText)rootView.findViewById(R.id.FP_Catatan);

        imgWA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //show
                GoToWA();
            }
        });

        imgCC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //GOTOCC
                GoToCC();
            }
        });


    }
    private void PasangSubTeknis(JSONArray data){
        try {
            PILIH_TEKNIS.clear();
            ID_TEKNIS.clear();
            for (int i=0; i< data.length();i++){
                ID_TEKNIS.add(data.getJSONObject(i).getString("id"));
                PILIH_TEKNIS.add(data.getJSONObject(i).getString("nama"));
            }
            Log.d(TAG, "PasangSubTeknis: Terpasang");
        }catch (JSONException e){

        }
    }
    private void PasangSubNonTeknis(JSONArray data){
        try {
            PILIH_NON_TEKNIS.clear();
            ID_NON_TEKNIS.clear();
            for (int i=0; i< data.length();i++){
                ID_NON_TEKNIS.add(data.getJSONObject(i).getString("id"));
                PILIH_NON_TEKNIS.add(data.getJSONObject(i).getString("nama"));
            }
            Log.d(TAG, "PasangSubNonTeknis: Terpasang");
        }catch (JSONException e){

        }
    }
    private void Req_Kategori(){
        showPDialog("Loading ...");
        // Creating volley request obj
        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_GetKategori, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();
                // Parsing json
                try {
                    JSONObject jObj = new JSONObject(response);
                    Boolean error = jObj.getBoolean("error");

                    if(!error){
                        PasangSubTeknis(jObj.getJSONObject("data").getJSONArray("teknis"));
                        PasangSubNonTeknis(jObj.getJSONObject("data").getJSONArray("non_teknis"));

                    }else{
                        Toast.makeText(getActivity(), jObj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // notifying list adapter about data changes
                // so that it renders the list view with updated data
                //adapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
//                finish();

            }
        });

        // Adding request to request queue
        strReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 10, 1.0f));
        AppController.getInstance().addToRequestQueue(strReq);
    }

    private void dialogKategori(){
        new MaterialDialog.Builder(getContext())
                .title("Kategori")
                .items(R.array.kategori)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        JenisKategori = position;
                        Id_Kategori = ""; //reset sub kategori

                        //Toast.makeText(getContext(), position+" - "+text, Toast.LENGTH_LONG).show();
                        tvKatPil.setText(text);
                        tvKatPil.setVisibility(View.VISIBLE);
                        //tvPilKat.setTextSize(12);
                        //tvPilKat.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));

                        //Reset For sub
                        //tvSubKatPil.setText(text);
                        //tvSubKatPil.setVisibility(View.GONE);
                        //tvPilSubKat.setTextSize(14);
                        //tvPilSubKat.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                    }
                })
                .show();
    }
    private void dialogSubTeknis(){
        new MaterialDialog.Builder(getContext())
                .title("Kategori Teknis")
                .items(PILIH_TEKNIS)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        Id_Kategori = ID_TEKNIS.get(position);
                        Log.d(TAG, "onSelection: "+Id_Kategori);

                        tvSubKatPil.setText(text);
                        tvSubKatPil.setVisibility(View.VISIBLE);
                        //tvPilSubKat.setTextSize(12);
                        //tvPilSubKat.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                    }
                })

                .show();
    }
    private void dialogSubNonTeknis(){
        new MaterialDialog.Builder(getContext())
                .title("Kategori Non Teknis")
                .items(PILIH_NON_TEKNIS)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        Id_Kategori = ID_NON_TEKNIS.get(position);
                        Log.d(TAG, "onSelection: "+Id_Kategori);

                        tvSubKatPil.setText(text);
                        tvSubKatPil.setVisibility(View.VISIBLE);
                        //tvPilSubKat.setTextSize(12);
                        //tvPilSubKat.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                    }
                })
                .show();
    }
    private void dialogFoto(){
        new MaterialDialog.Builder(getActivity())
                .title("Pilihan")
                .items(CHOOSE_FILE)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        if (position==0){
                            if (hasCameraPermission()){
                                takeFromCamera();
                            }else {
                                openCameraPermission();
                            }
                        }else {
                            galleryIntent();
                        }
                    }
                })
                .show();
    }
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }
    private boolean hasCameraPermission(){
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED) {
            return true;
        }else{
            return false;
        }
    }
    private void openCameraPermission(){
        requestPermissions(new String[]{Manifest.permission.CAMERA},
                Template.Code.CAMERA_IMAGE_CODE);
    }
    //Hasil request permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d("TAG", "dialog onRequestPermissionsResult");
        switch (requestCode) {
            case Template.Code.CAMERA_IMAGE_CODE:
                // Check Permissions Granted or not
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takeFromCamera();
                } else {
                    // Permission Denied
                    Toast.makeText(getActivity(), "Read Camera permission is denied", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    public boolean hasImageCaptureBug() {

        // list of known devices that have the bug
        ArrayList<String> devices = new ArrayList<String>();
        devices.add("android-devphone1/dream_devphone/dream");
        devices.add("generic/sdk/generic");
        devices.add("vodafone/vfpioneer/sapphire");
        devices.add("tmobile/kila/dream");
        devices.add("verizon/voles/sholes");
        devices.add("google_ion/google_ion/sapphire");

        return devices.contains(android.os.Build.BRAND + "/" + android.os.Build.PRODUCT + "/"
                + android.os.Build.DEVICE);

    }
    private void takeFromCamera(){
        //Mengambil foto dengan camera
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        //for run > android 6(Mars)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            mOutputUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    new ContentValues());
        }else {
            mOutputUri = FileManager.getOutputMediaFileUri(Template.Code.CAMERA_IMAGE_CODE);
        }

        if (hasImageCaptureBug()) {
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File("/sdcard/tmp")));
            Log.d(TAG, "takeFromCamera: Bug On");
        } else {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputUri);
            Log.d(TAG, "takeFromCamera: Bug Off");
            //intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        }

        Log.d(TAG, "takeFromCamera: "+mOutputUri);
        startActivityForResult(intent, Template.Code.CAMERA_IMAGE_CODE);
    }
    private void galleryIntent(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), Template.Code.FILE_MANAGER_CODE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == Template.Code.FILE_MANAGER_CODE){
                    //onSelectFromGalleryResult(data);
                    mOutputUri = data.getData();
                    //setFile(requestCode, mOutputUri);
                    //setView(requestCode, mOutputUri);
                    CompressImage(getFile(requestCode, mOutputUri));
                }
                else if (requestCode == Template.Code.CAMERA_IMAGE_CODE){
                    //onCaptureImageResult(data);
                    //setFile(requestCode, mOutputUri);
                    //setView(requestCode, mOutputUri);
                    CompressImage(getFile(requestCode, mOutputUri));
                }

            }
    }
    private String getFilename(File file){
        return file.getName();
    }
    public String getImagePath(Uri uri){
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        cursor.close();

        cursor = getActivity().getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }
    public File CompressImageFile(File file){
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE=75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }
    private File getFile(int type, Uri uri) {
        //File mediaStorageDir = new File(getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES), TAG);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            return new File(getImagePath(uri));
        }else {
            return new File(FileManager.getPath(getActivity().getApplicationContext(), type, uri));
        }
    }
    private void TampilImageAfterCompress(File file){
        Glide
                .with(getActivity())
                .load(file)
                .asBitmap()
                .placeholder(R.drawable.no_image)
                .into(imageView);
        imageView.setVisibility(ImageView.VISIBLE);
        imageView.getLayoutParams().height = 500;
        imageView.requestLayout();
        //hidden tulisan lampirkan foto
        txt_LampirFoto.setVisibility(View.GONE);

    }
    private void CompressImage(File srcImg){
        try {
//            Log.d(TAG, "Path Before Compress: "+srcImg.getAbsolutePath());
//            Log.d(TAG, "Size Before Compress: "+getFileSize(srcImg));

            //Toast.makeText(getActivity(), "Sebelum : "+getFileSize(srcImg), Toast.LENGTH_SHORT).show();

            //compress image hasil file
            compressedImageFile = new Compressor(getContext()).compressToFile(srcImg);
            isSetImage = true;

            //tampilkan image
            TampilImageAfterCompress(compressedImageFile);

//            Log.d(TAG, "Path After Compress: "+compressedImageFile.getAbsolutePath());
//            Log.d(TAG, "Size After Compress: "+getFileSize(compressedImageFile));
            //Toast.makeText(getActivity(), "Stelah : "+getFileSize(compressedImageFile), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private String getFileSize(File oldFile){
        File file = new File(oldFile.getAbsolutePath());
        int file_size = Integer.parseInt(String.valueOf(file.length()/1024));
        return  String.valueOf(file_size);
    }
    private void UploadFile(){

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(AppConfig.URL_Pengaduan)
                .post(requestBody)
                .build();

        /*
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        */


        try {
            client.newCall(request).enqueue(new Callback() {
                @Override public void onFailure(Call call, IOException e) {
                    hidePDialog();
                }

                @Override public void onResponse(Call call, okhttp3.Response response) throws IOException {
                    final String textResponse = response.body().string();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override public void run() {
                            Log.d(TAG, "run: "+textResponse);
                            try {
                                JSONObject jObj = new JSONObject(textResponse);
                                Boolean error = jObj.getBoolean("error");

                                if (!error){
                                    //JSONObject data = jObj.getJSONObject("data");
                                    responDialog(jObj.getString("message"));

                                }else{
                                    Toast.makeText(getActivity(), ""+jObj.getString("error_msg"), Toast.LENGTH_LONG).show();
                                }
                                hidePDialog();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private boolean isIdKategoriValid(){
        if (Id_Kategori.equals("") || Id_Kategori.isEmpty() || Id_Kategori.equals(null)){
            return false;
        }else {
            return true;
        }
    }
    private boolean isCatatanValid(){
        if (edCatatan.getText().toString().equals("")){
            return false;
        }else {
            return true;
        }
    }
    private void CekingForUpload(){
        if (isCatatanValid() & isIdKategoriValid()){
            if (UserHelper.GPS_Loc_Lat!=null && UserHelper.GPS_Loc_Lang!=null){
                if (isSetImage){
                    getDataLaporImage(Id_Kategori, edCatatan.getText().toString(), UserHelper.GPS_Loc_Lat,UserHelper.GPS_Loc_Lang, UserHelper.Nama);
                }else {
                    getDataLapor(Id_Kategori, edCatatan.getText().toString(), UserHelper.GPS_Loc_Lat,UserHelper.GPS_Loc_Lang, UserHelper.Nama);
                }
                UploadFile();
            }else {
                Toast.makeText(getActivity(), "Silahkan aktifkan gps anda", Toast.LENGTH_SHORT).show();
            }

        }else {
            Toast.makeText(getActivity(), "Periksa Data", Toast.LENGTH_SHORT).show();
        }
    }
    //kategori, long, lat,token, laporan, kontak, foto
    private void getDataLaporImage(final String kategori, final String laporan, final String loc_lat, final String loc_lang, final String kontak){
        requestBody =  new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                //.addFormDataPart("submit", "true")
                .addFormDataPart("kategori", kategori)
                .addFormDataPart("nama", UserHelper.Nama)
                .addFormDataPart("lat", loc_lat)
                .addFormDataPart("long", loc_lang)
                .addFormDataPart("token", UserHelper.UToken)
                .addFormDataPart("laporan", laporan)
                .addFormDataPart("kontak", kontak)

                .addFormDataPart("foto", getFilename(compressedImageFile),
                        RequestBody.create(MEDIA_TYPE_PNG, compressedImageFile))
                .build();
    }
    private void getDataLapor(final String kategori, final String laporan, final String loc_lat, final String loc_lang, final String kontak){
        requestBody =  new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                //.addFormDataPart("submit", "true")
                .addFormDataPart("kategori", kategori)
                .addFormDataPart("nama", UserHelper.Nama)
                .addFormDataPart("lat", loc_lat)
                .addFormDataPart("long", loc_lang)
                .addFormDataPart("token", UserHelper.UToken)
                .addFormDataPart("laporan", laporan)
                .addFormDataPart("kontak", kontak)

//                .addFormDataPart("foto", getFilename(compressedImageFile),
//                        RequestBody.create(MEDIA_TYPE_PNG, compressedImageFile))
                .build();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void DialogMessage(String message){
        new MaterialDialog.Builder(getActivity())
                .title("PERHATIAN")
                .content(message)
                .positiveText("Tutup")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                })
                .show();
    }
    private void responDialog(String message){
        new MaterialDialog.Builder(getActivity())
                .title("PERHATIAN")
                .content(message)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        getActivity().finish();
                    }
                })
                .show();
    }
    private void AskForUpload(){
        new MaterialDialog.Builder(getActivity())
                .title("Kirim Data")
                .content("apakah anda yakin akan mengirim data tersebut?")
                .positiveText("Kirim")
                .neutralText("Batal")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        showPDialog("Loading ...");
                        //getDataLapor(edJudulAduan.getText().toString(), edIsiAduan.getText().toString(), UserHelper.GPS_Loc_Lat, UserHelper.GPS_Loc_Lang);
                        //UploadFile();
                    }
                })
                .show();
    }

    private void GoToWA(){
        //WA https://api.whatsapp.com/send?phone=6281348500060
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://api.whatsapp.com/send?phone=6281348500060"));
        startActivity(i);
    }
    private void GoToCC(){
        //WA https://api.whatsapp.com/send?phone=6281348500060
        String uri = "tel:02476920999";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(uri));
        startActivity(i);
    }

}
