package com.dinustek.pdamsemarang.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.activity.DetailGangguanAirActivity;
import com.dinustek.pdamsemarang.model.item_gangguan_air;

import java.util.List;

/**
 * Created by ASA on 6/1/2017.
 */

public class adapter_gangguan_air extends RecyclerView.Adapter<adapter_gangguan_air.MyViewHolder> {
    private List<item_gangguan_air> ListGanguan;
    private Context mContext;
    int[] RString = {R.string.info_0, R.string.info_1, R.string.info_2, R.string.info_3, R.string.info_4, R.string.info_5, R.string.info_6, R.string.info_7, R.string.info_8, R.string.info_9};
    int[] RColor = {R.color.c1, R.color.c2, R.color.c3, R.color.c4, R.color.c5, R.color.c6, R.color.c7, R.color.c8, R.color.c9, R.color.c10 };
    //ImageLoader imageLoader;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvJudul, tvKonten, tvTanggal, tvLokasi, tvShare;
        public final RelativeLayout RL_Gangguan;


        public MyViewHolder(View view) {
            super(view);

            tvJudul = (TextView) view.findViewById(R.id.cvGA_Judul);
            tvKonten = (TextView) view.findViewById(R.id.cvGA_Content);
            tvLokasi = (TextView) view.findViewById(R.id.cvGA_Lokasi);
            tvTanggal = (TextView) view.findViewById(R.id.cvGA_Tanggal);
            tvShare = (TextView) view.findViewById(R.id.cvGA_edShare);
            RL_Gangguan = (RelativeLayout) view.findViewById(R.id.RL_GangguanAir);
        }
    }

    public adapter_gangguan_air(Context context, List<item_gangguan_air> Listgangguan) {
        mContext = context;
        this.ListGanguan= Listgangguan;
    }

    @Override
    public adapter_gangguan_air.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_gangguanair, parent, false);

        return new adapter_gangguan_air.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(adapter_gangguan_air.MyViewHolder holder, int position) {
        final item_gangguan_air item = ListGanguan.get(position);
        //imageLoader = AppController.getInstance().getImageLoader();

        holder.tvJudul.setText(item.getJudul());
        if (item.getKonten().length()>100){
            holder.tvKonten.setText(item.getKonten().substring(0, 100));
        }else {
            holder.tvKonten.setText(item.getKonten());
        }

        holder.tvLokasi.setText(item.getLokasi());
        holder.tvTanggal.setText(item.getTanggal());

        holder.RL_Gangguan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, "ID : "+item.getId(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(mContext, DetailGangguanAirActivity.class);
                i.putExtra("dga_judul", item.getJudul());
                i.putExtra("dga_konten", item.getKonten());
                i.putExtra("dga_lokasi", item.getLokasi());
                i.putExtra("dga_tanggal", item.getTanggal());
                view.getContext().startActivity(i);
//                DialogPesan("Detail Konten", item.getKonten());
            }
        });
        holder.tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareAction(item.getJudul(), item.getKonten(), item.getLokasi(), item.getTanggal());
            }
        });

    }
    private void ShareAction(String judul, String konten, String lokasi, String tanggal){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Android PDAM Kota Semarang\n"+judul+"\n"+konten+"\n"+lokasi+"\n"+tanggal;
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, judul);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        mContext.startActivity(Intent.createChooser(sharingIntent, "Bagikan dengan"));
    }
    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(mContext)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .show();
    }

    @Override
    public int getItemCount() {
        return ListGanguan.size();
    }
}
