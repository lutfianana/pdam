package com.dinustek.pdamsemarang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.activity.DetailBeritaActivity;
import com.dinustek.pdamsemarang.model.item_aduanku;
import com.dinustek.pdamsemarang.model.item_berita;

import java.util.List;

/**
 * Created by ASA on 6/20/2017.
 */

public class adapter_aduanku extends RecyclerView.Adapter<adapter_aduanku.MyViewHolder> {
    private List<item_aduanku> ListData;
    private Context mContext;
    int[] RString = {R.string.info_0, R.string.info_1, R.string.info_2, R.string.info_3, R.string.info_4, R.string.info_5, R.string.info_6, R.string.info_7, R.string.info_8, R.string.info_9};
    int[] RColor = {R.color.c1, R.color.c2, R.color.c3, R.color.c4, R.color.c5, R.color.c6, R.color.c7, R.color.c8, R.color.c9, R.color.c10 };
    //ImageLoader imageLoader;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvNama, tvTanggal, tvStatus, tvKategori, tvLaporan;
        public final ImageView cvAduanku_Img;
        public final CardView cv_Aduanku;


        public MyViewHolder(View view) {
            super(view);

            cvAduanku_Img = (ImageView)view.findViewById(R.id.cvAd_Image);
            tvNama = (TextView) view.findViewById(R.id.cvAd_Nama);
            tvTanggal = (TextView) view.findViewById(R.id.cvAd_Tanggal);
            tvLaporan = (TextView) view.findViewById(R.id.cvAd_Content);
            tvStatus = (TextView) view.findViewById(R.id.cvAd_Status);
            tvKategori = (TextView) view.findViewById(R.id.cvAd_Kategori);
            cv_Aduanku = (CardView)view.findViewById(R.id.cv_Aduanku);
        }
    }

    public adapter_aduanku(Context context, List<item_aduanku> ListDatas) {
        mContext = context;
        this.ListData= ListDatas;
    }

    @Override
    public adapter_aduanku.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_aduanku, parent, false);

        return new adapter_aduanku.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(adapter_aduanku.MyViewHolder holder, int position) {
        final item_aduanku item = ListData.get(position);
        //imageLoader = AppController.getInstance().getImageLoader();

        holder.tvNama.setText(item.getNama());
        holder.tvTanggal.setText(item.getTanggal());
        holder.tvLaporan.setText(item.getLaporan());
        holder.tvStatus.setText(item.getStatus());
        holder.tvKategori.setText(item.getKategori());
        holder.cv_Aduanku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, "Berita : "+item.getNoID(), Toast.LENGTH_SHORT).show();
//                Intent i = new Intent(mContext, DetailBeritaActivity.class);
//                i.putExtra("db_judul", item.getJudul());
//                i.putExtra("db_tanggal", item.getWaktu());
//                i.putExtra("db_konten", item.getKonten());
//                view.getContext().startActivity(i);
            }
        });

        //String Img_Url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR16g-yeRbqKiQXYpQEZCFHrNBogC3R1JVmLklpdMYDOjTXeZg7xA";
        if (!item.getFoto().equals("null")){
            Glide.with(mContext)
                    .load(item.getFoto())
                    .asBitmap()
                    .thumbnail(0.1f)
                    .into(holder.cvAduanku_Img);
        }else {
            holder.cvAduanku_Img.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return ListData.size();
    }
}