package com.dinustek.pdamsemarang.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.activity.DetailInfoActivity;
import com.dinustek.pdamsemarang.model.item_info;

import java.util.List;

/**
 * Created by ASA on 6/1/2017.
 */

public class adapter_info extends RecyclerView.Adapter<adapter_info.MyViewHolder> {
    private List<item_info> ListInfo;
    private Context mContext;
    int[] RString = {R.string.info_0, R.string.info_1, R.string.info_2, R.string.info_3, R.string.info_4, R.string.info_5, R.string.info_6, R.string.info_7, R.string.info_8, R.string.info_9};
    int[] RColor = {R.color.c1, R.color.c2, R.color.c3, R.color.c4, R.color.c5, R.color.c6, R.color.c7, R.color.c8, R.color.c9, R.color.c10 };
    //ImageLoader imageLoader;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tv_Name;
        public final CardView cv_Info;


        public MyViewHolder(View view) {
            super(view);
            
            tv_Name = (TextView) view.findViewById(R.id.cardinfo_name);
            cv_Info = (CardView)view.findViewById(R.id.cv_Info);
        }
    }

    public adapter_info(Context context, List<item_info> ListInfos) {
        mContext = context;
        this.ListInfo= ListInfos;
    }

    @Override
    public adapter_info.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_beranda, parent, false);

        return new adapter_info.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(adapter_info.MyViewHolder holder, int position) {
        final item_info item = ListInfo.get(position);
        //imageLoader = AppController.getInstance().getImageLoader();

        holder.tv_Name.setText(mContext.getString(RString[Integer.valueOf(item.getNoId())]));
        holder.cv_Info.setCardBackgroundColor(ContextCompat.getColor(mContext, RColor[Integer.valueOf(item.getNoId())]));


        holder.cv_Info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, "Berita"+item.getNewsId(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(mContext, DetailInfoActivity.class);
                i.putExtra("url", "http://dinustech.com/demoapp/tirta/android-pasangbaru.php");
                view.getContext().startActivity(i);
            }
        });
        /*
        Glide.with(mContext)
                .load(gambar[Integer.valueOf(item.getNewsId())%5])
                .asBitmap()
                .fitCenter()
                .into(holder.tvB_Image);
                */

    }

    @Override
    public int getItemCount() {
        return ListInfo.size();
    }
}
