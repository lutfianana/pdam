package com.dinustek.pdamsemarang.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.activity.DetailTagihanActivity;
import com.dinustek.pdamsemarang.model.item_tagihan_belum;

import java.util.List;

/**
 * Created by ASA on 6/20/2017.
 */

public class adapter_tagihan_belum extends RecyclerView.Adapter<adapter_tagihan_belum.MyViewHolder> {
    private List<item_tagihan_belum> ListBerita;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvBulan, tvHarga;
        public final RelativeLayout cv_T_RL;


        public MyViewHolder(View view) {
            super(view);
            
            tvBulan = (TextView) view.findViewById(R.id.cv_T_bulan);
            tvHarga = (TextView) view.findViewById(R.id.cv_T_rupiah);
            cv_T_RL = (RelativeLayout) view.findViewById(R.id.cv_T_RL);
        }
    }

    public adapter_tagihan_belum(Context context, List<item_tagihan_belum> ListBeritas) {
        mContext = context;
        this.ListBerita= ListBeritas;
    }

    @Override
    public adapter_tagihan_belum.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_tagihan_belum, parent, false);

        return new adapter_tagihan_belum.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(adapter_tagihan_belum.MyViewHolder holder, int position) {
        final item_tagihan_belum item = ListBerita.get(position);
        //imageLoader = AppController.getInstance().getImageLoader();

        holder.tvBulan.setText(item.getBulan());
        holder.tvHarga.setText(item.getHarga());
        holder.cv_T_RL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, DetailTagihanActivity.class);
                i.putExtra("tahun", item.getPeriode().substring(0, 4));
                i.putExtra("bulan", item.getPeriode().substring(4, 6));
                i.putExtra("nopel", "");
                i.putExtra("bayar", false);
                mContext.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return ListBerita.size();
    }
}