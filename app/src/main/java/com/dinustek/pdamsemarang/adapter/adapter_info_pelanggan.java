package com.dinustek.pdamsemarang.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.model.item_info_pelanggan;

import java.util.List;

/**
 * Created by ASA on 6/20/2017.
 */

public class adapter_info_pelanggan extends RecyclerView.Adapter<adapter_info_pelanggan.MyViewHolder> {
    private List<item_info_pelanggan> ListBerita;
    private Context mContext;
    int[] RString = {R.string.info_0, R.string.info_1, R.string.info_2, R.string.info_3, R.string.info_4, R.string.info_5, R.string.info_6, R.string.info_7, R.string.info_8, R.string.info_9};
    //int[] RColor = {R.color.c1, R.color.c2, R.color.c3, R.color.c4, R.color.c5, R.color.c6, R.color.c7, R.color.c8, R.color.c9, R.color.c10 };


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvJudul;
        public final RelativeLayout cip_Parent;


        public MyViewHolder(View view) {
            super(view);

            cip_Parent = (RelativeLayout) view.findViewById(R.id.cip_Parent);
            tvJudul = (TextView) view.findViewById(R.id.cip_tvNama);
        }
    }

    public adapter_info_pelanggan(Context context, List<item_info_pelanggan> ListBeritas) {
        mContext = context;
        this.ListBerita= ListBeritas;
    }

    @Override
    public adapter_info_pelanggan.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_informasi_pelanggan, parent, false);

        return new adapter_info_pelanggan.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(adapter_info_pelanggan.MyViewHolder holder, int position) {
        final item_info_pelanggan item = ListBerita.get(position);
        //imageLoader = AppController.getInstance().getImageLoader();

        holder.tvJudul.setText(item.getNama());
        holder.cip_Parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Goto(item.getURL());
            }
        });


    }
    @Override
    public int getItemCount() {
        return ListBerita.size();
    }
    private void Goto(String url){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        mContext.startActivity(i);
    }
}