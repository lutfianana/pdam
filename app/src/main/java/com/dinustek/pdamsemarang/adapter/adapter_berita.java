package com.dinustek.pdamsemarang.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dinustek.pdamsemarang.R;
import com.dinustek.pdamsemarang.activity.DetailBeritaActivity;
import com.dinustek.pdamsemarang.activity.DetailInfoActivity;
import com.dinustek.pdamsemarang.model.item_berita;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ASA on 6/20/2017.
 */

public class adapter_berita extends RecyclerView.Adapter<adapter_berita.MyViewHolder> {
    private List<item_berita> ListBerita;
    private Context mContext;
    int[] RString = {R.string.info_0, R.string.info_1, R.string.info_2, R.string.info_3, R.string.info_4, R.string.info_5, R.string.info_6, R.string.info_7, R.string.info_8, R.string.info_9};
    int[] RColor = {R.color.c1, R.color.c2, R.color.c3, R.color.c4, R.color.c5, R.color.c6, R.color.c7, R.color.c8, R.color.c9, R.color.c10 };
    //ImageLoader imageLoader;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvJudul, tvWaktu;
        public final ImageView cvBerita_Img;
        public final CardView cv_Berita;


        public MyViewHolder(View view) {
            super(view);

            cvBerita_Img = (ImageView)view.findViewById(R.id.cvBerita_img);
            tvJudul = (TextView) view.findViewById(R.id.cvBerita_Judul);
            tvWaktu = (TextView) view.findViewById(R.id.cvBerita_Waktu);
            cv_Berita = (CardView)view.findViewById(R.id.cv_Berita);
        }
    }

    public adapter_berita(Context context, List<item_berita> ListBeritas) {
        mContext = context;
        this.ListBerita= ListBeritas;
    }

    @Override
    public adapter_berita.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_berita, parent, false);

        return new adapter_berita.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(adapter_berita.MyViewHolder holder, int position) {
        final item_berita item = ListBerita.get(position);
        //imageLoader = AppController.getInstance().getImageLoader();

        holder.tvJudul.setText(item.getJudul());
        holder.tvWaktu.setText(item.getWaktu());
        holder.cv_Berita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, "Berita : "+item.getNoID(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(mContext, DetailBeritaActivity.class);
                i.putExtra("db_judul", item.getJudul());
                i.putExtra("db_tanggal", item.getWaktu());
                i.putExtra("db_konten", item.getKonten());
                view.getContext().startActivity(i);
            }
        });

        String Img_Url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR16g-yeRbqKiQXYpQEZCFHrNBogC3R1JVmLklpdMYDOjTXeZg7xA";
        Glide.with(mContext)
                .load(Img_Url)
                .thumbnail(0.1f)
                .into(holder.cvBerita_Img);

    }

    @Override
    public int getItemCount() {
        return ListBerita.size();
    }
}